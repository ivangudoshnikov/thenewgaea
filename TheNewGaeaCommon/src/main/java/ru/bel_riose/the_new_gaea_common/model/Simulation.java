package ru.bel_riose.the_new_gaea_common.model;

import ru.bel_riose.the_new_gaea_common.model.transferable.TransferableContainer;
import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Bel_Riose
 */
public class Simulation extends TransferableContainer implements Serializable, Cloneable {

    private Level root;

    public Level getRoot() {
        return root;
    }

    public void setRoot(Level root) {
        this.root = root;
    }

    public void update(double dt) {
        clearUpdated();
        root.update(dt);
    }

    public static Simulation LoadFromFile(String filename) throws FileNotFoundException, IOException, ClassNotFoundException {
        //standart binary java serializer
//        FileInputStream fis = new FileInputStream(filename);
//        ObjectInputStream in = new ObjectInputStream(fis);
//        Simulation result = (Simulation) in.readObject();
//        in.close();
//        return result;
        FileInputStream fis = new FileInputStream(filename);
        JsonReader jsonReader = new JsonReader(fis);
        return (Simulation) jsonReader.readObject();

    }

    public void SaveToFile(String filename) throws IOException {
        //standart binary java serializer
//        FileOutputStream fos = null;
//        ObjectOutputStream out = null;
//        fos = new FileOutputStream(filename);        
//        out = new ObjectOutputStream(fos);
//        out.writeObject(this);
//        out.close(); 
        FileOutputStream fos = new FileOutputStream(filename);
        PrintWriter pw = new PrintWriter(fos);
        pw.print(writeAllJsonIo(this));
        pw.close();
    }

    static public String writeAllJsonIo(Simulation sim) {
        String result = null;
        try {
            final HashMap<String, Object> parameters = new HashMap();
            parameters.put(JsonWriter.PRETTY_PRINT, true);
            result = JsonWriter.objectToJson(sim, parameters);
        } catch (IOException ex) {
            Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return result;
    }

    static public Simulation readAllJsonIo(String s) throws IOException {
        return (Simulation) JsonReader.jsonToJava(s);
    }

    public class JSON_KEYS extends TransferableContainer.JSON_KEYS {

        public static final String rootLevelId = "rootLevelId";
    }

    @Override
    public JSONObject writeJsonOnlyUpdated() {
        JSONObject result = super.writeJsonOnlyUpdated();
        result.put(JSON_KEYS.rootLevelId, root.getId());
        return result;
    }

    @Override
    public void updateByJson(JSONObject json) {
        super.updateByJson(json); 
        root=(Level) getEntity((Long)json.get(JSON_KEYS.rootLevelId));
    }

    
    @Override
    public Object clone() {
        Simulation s = null;
        try {
            s = (Simulation) super.clone();
            s = readAllJsonIo(writeAllJsonIo(s));
        } catch (CloneNotSupportedException | IOException ex) {
        }
        return s;
    }
}
