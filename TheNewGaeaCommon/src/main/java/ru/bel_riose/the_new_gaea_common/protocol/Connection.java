/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.protocol;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ivan
 */
public class Connection {

    private final Socket s;
    private final ObjectInputStream i;
    private final ObjectOutputStream o;

    public Connection(Socket s) {
        this.s = s;
        ObjectOutputStream tempObjOS=null;
        ObjectInputStream tempObjIS=null;
        try { 
            tempObjOS = new ObjectOutputStream(s.getOutputStream());
            tempObjIS = new ObjectInputStream(s.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        o=tempObjOS;
        i=tempObjIS;
    }

    /**At least I can write safely
     * 
     * @param s
     * @throws IOException 
     */
    public synchronized void write(String s) throws IOException {
        o.writeUTF(s);
        o.flush();
        o.reset();
    }
    
    public int available() throws IOException{
        return i.available();
    }
    
    public String read() throws IOException{
        return i.readUTF();
    }
    
    public String getAddress(){
        return s.getInetAddress().getHostAddress();
    }
    
    public void disconnect() {        
        try {
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
