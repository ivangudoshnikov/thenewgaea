package ru.bel_riose.the_new_gaea_common.model.transferable;

import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 *
 * @author Ivan
 */
public abstract class TransferableContainer {

    //общая структура
    private long freeId = 0;
    private final HashMap<Long, TransferableEntity> entities = new HashMap<Long, TransferableEntity>();
        
    //готовые к отправке сущности
    private final JSONObject updated = new JSONObject();
        
    //анализатор json-a
    private final EntityUpdater entityUpdater = new EntityUpdater(this,entities);

    /**
     * @param entity
     * @return id
     */
    long register(TransferableEntity entity) {
        entities.put(freeId, entity);
        return freeId++;
    }

    public TransferableEntity getEntity(long id) {
        return entities.get(id);
    }

    public void registerUpdate(TransferableEntity entity) {
        updated.put(entity.getId(), entity.toJson());
    }

    void unregister(TransferableEntity entity) {
        entities.remove(entity.getId());
        updated.put(entity.getId(), null);
    }

    public void clearUpdated() {
        updated.clear();
    }

    public class JSON_KEYS {

        public static final String freeId = "freeId",
                updated = "updated";
    }

    public JSONObject writeJsonOnlyUpdated() {
        JSONObject result = new JSONObject();
        result.put(JSON_KEYS.freeId, freeId);
        result.put(JSON_KEYS.updated, updated);
        return result;
    }

    public JSONObject writeJsonAll() {
        for (long key : entities.keySet()) {
            updated.put(key, entities.get(key).toJson());
        }
        return writeJsonOnlyUpdated();
    }

    public void updateByJson(JSONObject json) {
        //Заполняем счетчик id
        freeId = (Long) json.get(JSON_KEYS.freeId);

        //получаем список обновленных сущностей
        JSONObject incomingUpdate = (JSONObject) json.get(JSON_KEYS.updated);
        //начинаем обновление
        entityUpdater.newUpdate(incomingUpdate);        
        //для каждой из них
        for (Object id : incomingUpdate.keySet()) {
            entityUpdater.getUpdated(Long.valueOf((String)id));
        }
    }
}
