/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_common.model.macro;

import java.util.LinkedList;
import java.util.Random;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;
import ru.bel_riose.the_new_gaea_common.model.transferable.TransferableEntity;

/**
 *
 * @author Ivan
 */
public class TerrainMesh extends TransferableEntity{

    private LinkedList<Vector> layer1= new LinkedList<>();
    
    /**For json parsers*/
    public TerrainMesh() {
    }

    /** Conventional constructor, circle+random*/
    public TerrainMesh(Simulation simulation,double r) {
        super(simulation);
        Random random= new Random();
        for(int i=0; i<1000; i++){
            double h=r+r*random.nextGaussian()/500f;
            layer1.add(new Vector(h*Math.cos(2*Math.PI*(i/1000f)),h*Math.sin(2*Math.PI*(i/1000f))));
        }
    }

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instaiciator) {
        //super.updateByJson(json, instaiciator);
        JSONArray layerArray= (JSONArray)json.get(JSON_KEYS.layer1);
        layer1.clear();
        for(Object v:layerArray){
            layer1.add(Vector.fromJson((JSONObject) v));
        }        
    }

    @Override
    public JSONObject toJson() {
        JSONObject result= super.toJson();
        JSONArray layerArray= new JSONArray();
        for(Vector v:layer1){
            layerArray.add(Vector.toJson(v));
        }        
        result.put(JSON_KEYS.layer1, layerArray);
        return result;
    }
    
    public class JSON_KEYS extends TransferableEntity.JSON_KEYS{
        public final static String layer1="Layer1";
    }
    
    public final static String TYPE_NAME="TerrainMesh";
    @Override
    public String getType() {
        return TYPE_NAME;
    }

    public LinkedList<Vector> getLayer1() {
        return layer1;
    }
    
    
}
