/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;

/**
 *
 * @author Ivan
 */
public class SimUpdateMessageBuilder extends AbstractMessageBuilder{

    public final static String TYPE_NAME="SimUpdate";
    
    @Override
    public String getType() {
        return TYPE_NAME;
    }
    
    public class JSON_KEYS extends AbstractMessageBuilder.JSON_KEYS{
        public static final String sim="sim";        
    }
    
    public JSONObject toJson(Simulation sim, boolean full) {
        JSONObject result=super.toJson();
        JSONObject simJson= full?sim.writeJsonAll():sim.writeJsonOnlyUpdated();
        result.put(JSON_KEYS.sim, simJson);
        return result;
    }
}
