/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.map_screen;

import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenGUI;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.bel_riose.numericalmethods.ode.Euler;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.the_new_gaea_common.model.space.DiskCelestialObject;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.view.gui.Message;
import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;

/**
 *
 * @author Ivan
 */
public class SpaceParametersWindow extends Window {

    private TheNewGaeaTestDesk application;
    private TextField txtTimeSpeed, txtG;
    private Button btnUpdate, btnClearTrajectories,btnPause;
    private Label lblMethod;
    private Button btnSwitchMethod;
    private Label lblCenterName;
    private List lstObjects;
    private Button btnAdd;

    public SpaceParametersWindow(TheNewGaeaTestDesk application, final MapScreenGUI gui) {
        super("Space parameters", gui.getSkin());
        this.application = application;
        add(new Label("Time speed: ", gui.getSkin()));
        txtTimeSpeed = new TextField(String.valueOf(((Space) this.application.sim.getRoot()).getTimeSpeed()), gui.getSkin());
        add(txtTimeSpeed);
        row();
        add(new Label("G: ", gui.getSkin()));
        txtG = new TextField(String.valueOf(((Space) this.application.sim.getRoot()).getG()), gui.getSkin());
        add(txtG);
        row();        
        btnUpdate = new TextButton("UPDATE", gui.getSkin());
        btnUpdate.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                try {
                    double newTimeSpeed = Double.valueOf(txtTimeSpeed.getText());
                    double newG = Double.valueOf(txtG.getText());
                    ((Space) SpaceParametersWindow.this.application.sim.getRoot()).setTimeSpeed(newTimeSpeed);
                    ((Space) SpaceParametersWindow.this.application.sim.getRoot()).setG(newG);
                    SpaceParametersWindow.this.update();
                    gui.addSimState("Space updated");
                } catch (Exception ex) {
                    new Message("Error", "Can't update parameters", gui.getStage(), gui.getSkin());
                }
            }
        });
        add();
        add(btnUpdate);
        row();
        add(new Label("Center: ", gui.getSkin()));
        GameObject center=((Space) application.sim.getRoot()).getCenter();
        //TODO: something with this!!! 
        lblCenterName = new Label(center==null?"":((DiskCelestialObject)center).getName(), gui.getSkin());
        add(lblCenterName);
        row();
        add(new Label("Method: ", gui.getSkin()));
        lblMethod = new Label(((Space) SpaceParametersWindow.this.application.sim.getRoot()).getPhysicsSolverName(), gui.getSkin());
        add(lblMethod);
        row();
        btnSwitchMethod = new TextButton("Switch", gui.getSkin());
        btnSwitchMethod.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (((Space) SpaceParametersWindow.this.application.sim.getRoot()).getPhysicsSolverName().equals(new ODEWrapSolver(new Euler()).getName())) {
                    ((Space) SpaceParametersWindow.this.application.sim.getRoot()).setPhysics(new ODEWrapSolver(new RungeKutta4()));
                } else {
                    ((Space) SpaceParametersWindow.this.application.sim.getRoot()).setPhysics(new ODEWrapSolver(new Euler()));
                }
                SpaceParametersWindow.this.update();
                gui.addSimState("set to "+((Space) SpaceParametersWindow.this.application.sim.getRoot()).getPhysicsSolverName());
            }
        });
        add();
        add(btnSwitchMethod);
        row();        
        btnPause= new TextButton("Start/Stop", gui.getSkin());
        btnPause.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SpaceParametersWindow.this.application.setSimRunning(!SpaceParametersWindow.this.application.isSimRunning());
            }
        });
        add(btnPause);
        btnClearTrajectories = new TextButton("Clear paths", gui.getSkin());
        btnClearTrajectories.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SpaceParametersWindow.this.application.trajectories.clear();
            }
        });
        add(btnClearTrajectories);
        pack();
        gui.getStage().addActor(this);
        setPosition(gui.getStage().getWidth() - getWidth(), gui.getStage().getHeight() - getHeight());
    }

    final void update() {
        txtTimeSpeed.setText(String.valueOf(((Space) this.application.sim.getRoot()).getTimeSpeed()));
        txtG.setText(String.valueOf(((Space) this.application.sim.getRoot()).getG()));
        lblMethod.setText(((Space) SpaceParametersWindow.this.application.sim.getRoot()).getPhysicsSolverName());
        GameObject center = ((Space) application.sim.getRoot()).getCenter();
        if (center != null) {
            lblCenterName.setText(((DiskCelestialObject)((Space) application.sim.getRoot()).getCenter()).getName());
        } else {
            lblCenterName.setText("");
        }

    }
}
