/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.view.gui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 *
 * @author Bel_Riose
 */
public class Message extends Window{
    private class OkButtonListener extends ClickListener{
        Window window;

        public OkButtonListener(Window window, int button) {
            super(button);
            this.window = window;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            window.remove();
        }
    }
    
    public Message(String title,String text,Stage stage, Skin skin) {
        super(title, skin);
        Label lblText= new Label(text, skin);
        this.center();
        this.add(lblText);
        this.row();
        Button btnOk= new TextButton("OK", skin);
        btnOk.addListener(new OkButtonListener(this,Input.Buttons.LEFT));        
        this.add(btnOk);
        this.pack();
        this.setModal(true);        
        this.setPosition((stage.getWidth()-this.getWidth())/2, (stage.getHeight()-this.getHeight())/2);
        stage.addActor(this);
    }
    
    
}
