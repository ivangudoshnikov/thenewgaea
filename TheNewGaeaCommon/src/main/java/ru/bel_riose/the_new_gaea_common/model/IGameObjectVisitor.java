/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model;

import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;

/**
 *
 * @author Ivan
 */
public interface IGameObjectVisitor {
    public void visitPlanet(Planet planet);
    public void visitStar(Star star);
    public void visitCharacter(Character character);
    
}
