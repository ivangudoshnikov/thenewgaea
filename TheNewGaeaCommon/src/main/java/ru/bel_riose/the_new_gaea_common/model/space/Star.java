/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.space;

import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;


/**
 *
 * @author Bel_Riose
 */
public class Star extends DiskCelestialObject {

    double luminocity;

    /**for json parsers
    */
    public Star() {
    }
        
    /**Conventional constructor 1
     * 
     * @param name
     * @param r
     * @param m
     * @param vx
     * @param vy
     * @param rotV
     * @param x
     * @param y
     * @param angle
     * @param luminocity
     * @param level 
     */
    public Star(String name, double r, double m, double vx, double vy, double rotV, double x, double y, double angle,double luminocity, Level level) {
        super(name, r, m, vx, vy, rotV, x, y, angle, level);
        this.luminocity = luminocity;
    }

    /**Conventional constructor 2
     * 
     * @param name
     * @param pos
     * @param angle
     * @param speed
     * @param angleSpeed
     * @param r
     * @param m
     * @param luminocity
     * @param level 
     */
    public Star(String name, Vector pos,double angle, Vector speed,double angleSpeed, double r, double m,double luminocity, Level level) {
        super(name, pos,angle,speed,angleSpeed,r, m, level);
        this.luminocity = luminocity;
    }

    public double getLuminocity() {
        return luminocity;
    }

    public void setLuminocity(double luminocity) {
        this.luminocity = luminocity;
    }
    
    public static final String TYPE_NAME="Star";
    @Override
    public String getType() {
        return TYPE_NAME;
    }
    
    public class JSON_KEYS extends DiskCelestialObject.JSON_KEYS{
        public static final String luminocity="luminocity";
                
    }

    @Override
    public JSONObject toJson() {
        JSONObject result=super.toJson(); 
        result.put(JSON_KEYS.luminocity, luminocity);
        return result;
    }

    @Override
    public void updateByJson(JSONObject json,EntityUpdater instanciator) {
        super.updateByJson(json,instanciator); 
        luminocity=(double)json.get(JSON_KEYS.luminocity);
    }
    
    @Override
    public void update(double dt) {
    }

    @Override
    public void acceptVisitor(IGameObjectVisitor visitor) {
        visitor.visitStar(this);
    }
}
