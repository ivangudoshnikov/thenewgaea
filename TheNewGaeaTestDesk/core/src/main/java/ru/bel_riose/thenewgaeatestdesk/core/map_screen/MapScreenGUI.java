/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.map_screen;

import ru.bel_riose.the_new_gaea_common.view.gui.FileSelectDialog;
import ru.bel_riose.the_new_gaea_common.view.gui.Message;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import java.io.File;
import java.util.logging.Logger;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.view.gui.FileSaveDialog;
import ru.bel_riose.the_new_gaea_common.view.gui.FileSelectDialog.FileHandler;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;
import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;
import ru.bel_riose.thenewgaeatestdesk.core.TrajectoriesMap;
import ru.bel_riose.thenewgaeatestdesk.core.gui.AddMenuWindow;
import ru.bel_riose.thenewgaeatestdesk.core.gui.ParamWindowFactory.ParamWindow;
import ru.bel_riose.thenewgaeatestdesk.core.gui.SimsListWindow;

/**
 *
 * @author Bel_Riose
 */
public class MapScreenGUI extends GraphicalUserInterface<MapScreen> {

    private static final Logger log = Logger.getLogger("UserInterface");
    private Window winFlieButtons;
    private ParamWindow winObjectParam;
    private SimsListWindow winSimsList;
    private AddMenuWindow winAddMenu;
    private SpaceParametersWindow winSpaceParams;
    private GameObject selectedObject;    

    public MapScreenGUI(MapScreen mapScreen) {
        //����� �����
        super(mapScreen);        
        Button btnNew = new TextButton("New", getSkin());
        btnNew.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                screen.getApplication().setSimRunning(false);
                removeParamWindow();
                screen.getCamera2d().restoreDefaults();
                screen.getApplication().CreateDefaultWorld();
                update();
            }
        });

        Button btnLoad = new TextButton("Load", getSkin());
        btnLoad.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                screen.getApplication().setSimRunning(false);
                new FileSelectDialog(MapScreenGUI.this, new FileHandler() {
                    @Override
                    public void handle(File file) {
                        //������������ ��������� ����: �������� ��������� �� ���� ���
                        try {
                            Simulation loadedSimulation = Simulation.LoadFromFile(file.getAbsolutePath());
                            screen.getApplication().trajectories.clear();
                            setSelectedObject(null);
                            screen.getCamera2d().restoreDefaults();
                            screen.getApplication().sim = loadedSimulation;
                            addSimState(file.getName()+" loaded");
                            update();
                            
                        } catch (Exception e) {
                            //���� �� ���������� - ������� ���������
                            new Message("Error", "Can't load world from this file", stage, getSkin());
                        }
                    }
                });
            }
        });
        Button btnSave = new TextButton("Save", getSkin());
        btnSave.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                screen.getApplication().setSimRunning(false);
                new FileSaveDialog(MapScreenGUI.this, new FileHandler() {
                    @Override
                    public void handle(File file) {
                        //������������ ��������� ����: �������� ��������� �� ���� ���
                        try {
                            screen.getApplication().sim.SaveToFile(file.getAbsolutePath());
                        } catch (Exception e) {
                            //���� �� ���������� - ������� ���������
                            e.printStackTrace();
                            new Message("Error", "Can't save to this file", stage, getSkin());
                        }
                    }
                });
            }
        });
        Button btnAbout = new TextButton("About", getSkin());
        btnAbout.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Message("About", "The New Gaea Test Desk " + TheNewGaeaTestDesk.version + "\r\n(c) Ivan Gudoshnikov aka Bel_Riose, 2013-2014", stage, getSkin());
            }
        });
        Button btnExit = new TextButton("Exit", getSkin());
        btnExit.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.exit(0);
            }
        });

        winFlieButtons = new Window("File", getSkin());
        winFlieButtons.add(btnNew);
        winFlieButtons.add(btnLoad);
        winFlieButtons.add(btnSave);
        winFlieButtons.add(btnAbout);
        winFlieButtons.add(btnExit);
        winFlieButtons.pack();
        winFlieButtons.setPosition(0, stage.getHeight());
        stage.addActor(this.winFlieButtons);

        winSimsList = new SimsListWindow(mapScreen,this);
        addSimState("oridgin");
        winSpaceParams = new SpaceParametersWindow(screen.getApplication(), this);        
    }

    private void showParamWindow(GameObject object) {
        removeParamWindow();
        winObjectParam = screen.paramWindowFacrory.getParamWindow(object, this);
        winObjectParam.pack();
        winObjectParam.setPosition(0, 0);
        stage.addActor(winObjectParam);
    }

    private void removeParamWindow() {
        if (winObjectParam != null) {
            winObjectParam.remove();
            winObjectParam = null;
        }
    }

    public void showAddMenuWindow(int screenX, int screenY) {
        removeAddMenuWindow();
        winAddMenu = new AddMenuWindow(screen, this, screenX, screenY);
    }

    public void removeAddMenuWindow() {
        if (winAddMenu != null) {
            winAddMenu.remove();
            winAddMenu = null;
        }
    }

    @Override
    public void update() {
        if (winObjectParam != null) {
            winObjectParam.update();
        }
        winSpaceParams.update();
    }

    public void setSelectedObject(GameObject object) {
        selectedObject = object;
        if (object != null) {
            showParamWindow(object);
        } else {
            removeParamWindow();
        }        
    }
    
    public GameObject getSelectedObject() {
        return selectedObject;
    }
    
    public void addSimState(String caption){
        winSimsList.addSimState(caption, screen.getApplication().sim, screen.getApplication().trajectories);
 
    }
}
