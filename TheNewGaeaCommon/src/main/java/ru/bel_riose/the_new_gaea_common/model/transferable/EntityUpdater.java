/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.transferable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.macro.TerrainMesh;

/**
 *
 * @author Ivan
 */
public class EntityUpdater {

    private Set<Long> alreadyUpdated = new HashSet<Long>();
    private final HashMap<Long, TransferableEntity> entities;
    private JSONObject jsonUpdate;
    private TransferableContainer container;

    public EntityUpdater(TransferableContainer container, HashMap<Long, TransferableEntity> entities) {
        this.entities = entities;
        this.container = container;
    }

    public void newUpdate(JSONObject jsonUpdate) {
        alreadyUpdated.clear();
        this.jsonUpdate = jsonUpdate;
    }

    private class UnknownEntityTypeException extends RuntimeException {
    }

    public TransferableEntity getUpdated(long id) {
        TransferableEntity result;
        if (!jsonUpdate.containsKey(String.valueOf(id))) {
            result = entities.get(id);
        } else if (alreadyUpdated.contains(id)) {
            result = entities.get(id);
        } else {
            JSONObject jsonEntity = (JSONObject) jsonUpdate.get(String.valueOf(id));
            if (jsonEntity == null) {
                //команда на удаление
                entities.get(id).delete();
                result = null;
            } else if (entities.containsKey(id)) {
                //обновление существующего объекта, возможно уже кем-то рекурсивно создан
                result = entities.get(id);
                if (!alreadyUpdated.contains(id)) {
                    result.updateByJson(jsonEntity, this);
                }
            } else {
                //создание новой сущности
                result = getNewInstance(id, jsonEntity);
                entities.put(id, result);
            }
            alreadyUpdated.add(id);
        }
        return result;
    }

    private TransferableEntity getNewInstance(long id, JSONObject json) {
        String type = (String) json.get(TransferableEntity.JSON_KEYS.type);
        TransferableEntity result = null;
        switch (type) {
            case Space.TYPE_NAME:
                result = new Space();
                break;
            case Planet.TYPE_NAME:
                result = new Planet();
                break;
            case Star.TYPE_NAME:
                result = new Star();
                break;
            case Character.TYPE_NAME:
                result = new Character();
                break;
            case PlanetSurface.TYPE_NAME:
                result = new PlanetSurface();
                break;
            case TerrainMesh.TYPE_NAME:
                result = new TerrainMesh();
                break;
            default:
                throw new UnknownEntityTypeException();
        }
        result.instanciateByJson(id, container, json, this);
        return result;
    }
}
