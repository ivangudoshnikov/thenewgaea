/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.macro;

import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;

/**
 *
 * @author Bel_Riose
 */
public class Character extends GameObject {
    public static double bbWidth=17,bbHeight=40;

    private double scale;
    
    /**for serialization*/
    public Character() {
    }

    /**Conventional constructor 1
     * 
     * @param x
     * @param y
     * @param angle
     * @param scale
     * @param level 
     */
    public Character(double x, double y, double angle, double scale,Level level) {
        this(new Vector(x, y), angle, new Vector(0, 0), 0,scale, level);
    }

    /**Conventional constructor 2
     * 
     * @param pos
     * @param angle
     * @param speed
     * @param angleSpeed
     * @param scale
     * @param level 
     */
    public Character(Vector pos, double angle, Vector speed, double angleSpeed,double scale, Level level) {
        super(pos, angle, speed, angleSpeed, level);
        this.scale=scale;
    }

    @Override
    public void update(double dt) {
    }
    
    public double getScale() {
        return scale;
    }

    @Override
    public boolean isInside(Vector p) {
        return false;
    }

    public static final String TYPE_NAME = "Character";

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    @Override
    public void acceptVisitor(IGameObjectVisitor visitor) {
        visitor.visitCharacter(this);
    }
    
    public class JSON_KEYS extends GameObject.JSON_KEYS{
        public static final String scale="scale";
    } 

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instanciator) {
        super.updateByJson(json, instanciator); 
        scale=(double)json.get(JSON_KEYS.scale);
    }

    @Override
    public JSONObject toJson() {
        JSONObject result= super.toJson();
        result.put(JSON_KEYS.scale, scale);
        return result;
    }
}
