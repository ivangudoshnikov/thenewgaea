/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_client.core.macro_screen;

import com.badlogic.gdx.Input;
import ru.bel_riose.libgdx_2d_skeleton.InputHandler;

/**
 *
 * @author Ivan
 */
class MacroScreenInputHandler extends InputHandler<MacroScreen>{

    public MacroScreenInputHandler(MacroScreen screen) {
        super(screen);
    }
    
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ESCAPE:
                System.exit(0);
                break;
            case Input.Keys.TAB:
                screen.getApplication().switchScreen();
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int i) {
        return true;
    }

    @Override
    public boolean keyTyped(char c) {
        return true;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return true;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return true;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return true;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        screen.getCamera2d().setK(screen.getCamera2d().getK() * Math.exp(-amount * 0.3));
        return true;
    }
    
}
