/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_server;

import java.net.Socket;
import ru.bel_riose.the_new_gaea_common.protocol.Connection;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

/**
 *
 * @author Ivan
 */
class ClientConnection extends Connection {

    boolean loggedIn;
    private Profile profile;

    public ClientConnection(Socket s) {
        super(s);
        loggedIn = false;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profiile) {
        this.profile = profiile;
    }

    @Override
    public void disconnect() {
        if (profile != null) {
            profile.setConnection(null);
        }
        profile = null;
        super.disconnect();
    }

}
