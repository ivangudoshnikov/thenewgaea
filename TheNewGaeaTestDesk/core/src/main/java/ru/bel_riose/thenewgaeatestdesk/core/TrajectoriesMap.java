/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core;

import java.util.HashMap;
import java.util.LinkedList;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.thenewgaeatestdesk.core.TrajectoriesMap.Trajectory;

/**
 *
 * @author Bel_Riose
 */
public class TrajectoriesMap extends HashMap<Long, Trajectory> implements Cloneable {

    public static class Trajectory extends LinkedList<Vector> implements Cloneable {

        @Override
        public Object clone() {
            Trajectory result = (Trajectory) super.clone();
            if (result != null) {
                result.clear();
                for (Vector v : this) {
                    result.add((Vector) v.clone());
                }
            }
            return result;
        }
    }

    @Override
    public Object clone() {
        TrajectoriesMap result = (TrajectoriesMap) super.clone();
        if (result != null) {
            result.clear();
            for (Long key : keySet()) {
                result.put(key, (Trajectory) get(key).clone());
            }
        }
        return result;
    }
}
