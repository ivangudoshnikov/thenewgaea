/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_server;

import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.protocol.AbstractMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.CurrentCharacterMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.LoginMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.LoginRejectMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.LoginSuccessMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;
import ru.bel_riose.the_new_gaea_common.protocol.SimUpdateMessageBuilder;

class UnknownMessageTypeException extends RuntimeException {
}

/**
 *
 * @author Ivan
 */
public class MessageManager {

    private static final SimUpdateMessageBuilder simUpdateMessageBuilder = new SimUpdateMessageBuilder();
    private static final CurrentCharacterMessageBuilder currentCharacterMessageBuilder = new CurrentCharacterMessageBuilder();
    private static final LoginSuccessMessageBuilder loginSuccessMessageBuilder = new LoginSuccessMessageBuilder();
    private static final LoginRejectMessageBuilder loginRejectedMessageBuilder = new LoginRejectMessageBuilder();

    public static JSONObject getSimUpdateMessage(Simulation sim, boolean full) {
        return simUpdateMessageBuilder.toJson(sim, full);
    }

    public static JSONObject getProfileUpdateMessage(Profile profile) {
        return currentCharacterMessageBuilder.toJson(profile);
    }

    public static JSONObject getLoginSuccessMessage(Profile profile,Simulation sim) {
        return loginSuccessMessageBuilder.toJson(profile,sim);
    }

    public static JSONObject getLoginRejectedMessage(String reason) {
        return loginRejectedMessageBuilder.toJson(reason);
    }

    static void processMessage(JSONObject message, ClientConnection connection, Server server) {
        String type = (String) message.get(AbstractMessageBuilder.JSON_KEYS.type);
        switch (type) {
            //сообщение о логине
            case LoginMessageBuilder.TYPE_NAME:
                server.login(message, connection);
                break;
            default:
                throw new UnknownMessageTypeException();
        }

    }
}
