/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core.map_screen;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import ru.bel_riose.the_new_gaea_client.core.map_screen.InfoWindowFactory.InfoWindow;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Bel_Riose
 */
class PlanetInfoWindow extends InfoWindow<Planet> {

    Label lblName, lblM, lblR;

    public PlanetInfoWindow(Planet planet, final GraphicalUserInterface gui) {
        super(planet, "Planet info", gui);

        add(new Label("Name: ", gui.getSkin()));
        lblName = new Label(planet.getName(), gui.getSkin());
        add(lblName);
        row();

        add(new Label("Mass: ", gui.getSkin()));
        lblM = new Label(String.valueOf(planet.getM()), gui.getSkin());
        add(lblM);
        row();

        add(new Label("Radius: ", gui.getSkin()));
        lblR = new Label(String.valueOf(planet.getR()), gui.getSkin());
        add(lblR);
    }

    @Override
    public void update(Simulation sim) {
        lblName.setText(object.getName());
        lblM.setText(String.valueOf(object.getM()));
        lblR.setText(String.valueOf(object.getR()));
    }
}

