/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import java.util.Iterator;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.libgdx_2d_skeleton.Camera2D;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.ILevelVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;

/**
 *
 * @author Ivan
 */
public class MacroDrawer {

    private final DrawerImpl impl = new DrawerImpl();
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private Camera2D camera;

    private static class Textures {

        private static Texture atlas = new Texture(Gdx.files.classpath("sprites/man1 - res.png"));
        final static TextureRegion character = new TextureRegion(atlas, 0, 24, 17, 40);
    }

    /**
     * @param gameObject
     * @param shapeRenderer tool
     * @param camera 
     */
    public void draw(GameObject gameObject, ShapeRenderer shapeRenderer, SpriteBatch spriteBatch, Camera2D camera) {
        this.shapeRenderer = shapeRenderer;
        this.spriteBatch = spriteBatch;
        this.camera = camera;
        
        Matrix4 modelMatrix = new Matrix4().translate((float) gameObject.getPos().x, (float) gameObject.getPos().y, 0);
        modelMatrix.rotate(0, 0, 1, -(float) Math.toDegrees(gameObject.getAngle()));
        Matrix4 modelViewProjMatrix = camera.getViewProjectionMatrix().mul(modelMatrix);
        
        this.spriteBatch.setProjectionMatrix(modelViewProjMatrix);
        this.shapeRenderer.setProjectionMatrix(modelViewProjMatrix);
        gameObject.acceptVisitor(impl);
        this.spriteBatch.setProjectionMatrix(camera.getViewProjectionMatrix());
        this.shapeRenderer.setProjectionMatrix(camera.getViewProjectionMatrix());
    }

    public void draw(Level level, ShapeRenderer shapeRenderer, SpriteBatch spriteBatch, Camera2D camera) {
        this.shapeRenderer = shapeRenderer;
        this.spriteBatch = spriteBatch;
        this.camera = camera;
        level.acceptVisitor(impl);
    }

    private class DrawerImpl implements IGameObjectVisitor, ILevelVisitor {

        @Override
        public void visitPlanet(Planet planet) {
            Vector coord = planet.getPos();
            shapeRenderer.setColor(0.5f, 0.2f, 0, 1);
            if (camera.rescaleMathToScr(planet.getR()) > 1) {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(0, 0, (float) planet.getR(), 20);
                shapeRenderer.end();
            } else {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
                shapeRenderer.point(0, 0, 0);
                shapeRenderer.end();
            }
        }

        @Override
        public void visitStar(Star star) {
            Vector coord = star.getPos();
            double r = star.getR();
            shapeRenderer.setColor(1, 1, 0, 1);
            if (camera.rescaleMathToScr(r + star.getLuminocity()) > 1) {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(0, 0, (float) (r + star.getLuminocity()), 20);
                shapeRenderer.setColor(1, 0.8f, 0, 1);
                shapeRenderer.circle(0, 0, (float) r, 20);
                shapeRenderer.end();
            } else {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
                shapeRenderer.point(0, 0, 0);
                shapeRenderer.end();
            }
        }

        @Override
        public void visitCharacter(Character character) {
            spriteBatch.begin();
            spriteBatch.draw(Textures.character, 0, 0, (float) (Character.bbWidth * character.getScale()), (float) (Character.bbHeight * character.getScale()));
            spriteBatch.end();
        }

        @Override
        public void visitSpace(Space space) {
        }

        @Override
        public void visitPlanetSurface(PlanetSurface surface) {
            Iterator<Vector> layer = surface.getTerrainMesh().getLayer1().iterator();
            Vector prev = layer.next();
            Vector current;
            shapeRenderer.setColor(1, 1, 1, 1);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            while (layer.hasNext()) {
                current = layer.next();
                shapeRenderer.line((float) prev.x, (float) prev.y, (float) current.x, (float) current.y);
                prev = current;
            }
            current = surface.getTerrainMesh().getLayer1().getFirst();
            shapeRenderer.line((float) prev.x, (float) prev.y, (float) current.x, (float) current.y);
            shapeRenderer.end();
        }
    }
}
