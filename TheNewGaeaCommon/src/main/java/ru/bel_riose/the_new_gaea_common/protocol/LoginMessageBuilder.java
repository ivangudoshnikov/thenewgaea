/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;

/**
 *
 * @author Ivan
 */
public class LoginMessageBuilder extends AbstractMessageBuilder{
    public final static String TYPE_NAME="Login";
    
    @Override
    public String getType() {
        return TYPE_NAME;
    }
    
    public class JSON_KEYS extends AbstractMessageBuilder.JSON_KEYS{
        public final static String name="name", password="password";
    }
    
    public JSONObject toJson(Profile profile){
        JSONObject result= super.toJson();
        result.put(JSON_KEYS.name, profile.getName());
        result.put(JSON_KEYS.password, profile.getPassword());
        return result;
    }
}
