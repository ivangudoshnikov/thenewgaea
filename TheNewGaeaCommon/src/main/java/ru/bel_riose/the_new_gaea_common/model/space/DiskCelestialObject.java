/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.space;

import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;


/**
 *
 * @author Bel_Riose
 */
public abstract class DiskCelestialObject extends GameObject {
    private double r, m; //радиус, масса
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public double getR() {
        return r;
    }

    public double getM() {
        return m;
    }


    public void setR(double r) {
        this.r = r;
    }

    public void setM(double m) {
        this.m = m;
    }

    @Override
    public boolean isInside(Vector p){        
        Vector pos=getPos();
        return (Math.hypot(p.x-pos.x, p.y-pos.y)<r);
    }
    
    /**for json-io parsers
    */
    protected DiskCelestialObject() {
    }
    
    
   /**
    * 
    * @param name
    * @param r
    * @param m
    * @param vx
    * @param vy
    * @param rotV
    * @param x
    * @param y
    * @param angle
    * @param enviroment 
    */
    public DiskCelestialObject(String name, double r, double m, double vx, double vy, double angleV, double x, double y, double angle, Level level) {
        this(name, new Vector(x, y),angle, new Vector(vx, vy),angleV, r, m, level);
    }
/**
 * 
 * @param name
 * @param pos
 * @param speed
 * @param r
 * @param m
 * @param enviroment 
 */
    public DiskCelestialObject(String name, Vector pos,double angle, Vector speed,double angleSpeed, double r, double m, Level level) {
        super(pos,angle, speed,angleSpeed, level);
        this.name = name;
        this.r = r;
        this.m = m;
    }

    public class JSON_KEYS extends GameObject.JSON_KEYS{
        public static final String m="m",
                name="name",
                r="r";
    }
    
    @Override
    public JSONObject toJson() {
         JSONObject result=super.toJson();
         result.put(JSON_KEYS.m, m);
         result.put(JSON_KEYS.name, name);
         result.put(JSON_KEYS.r, r);
         return result;         
    }

    @Override
    public void updateByJson(JSONObject json,EntityUpdater instanciator) {
        super.updateByJson(json,instanciator); 
        m=(double)json.get(JSON_KEYS.m);
        r=(double)json.get(JSON_KEYS.r);
        name=(String)json.get(JSON_KEYS.name);
    }
    
    
}