package ru.bel_riose.the_new_gaea_common.model.transferable;

import org.json.simple.JSONObject;

/**
 *
 * @author Ivan
 */
public abstract class TransferableEntity {

    private long id;
    private TransferableContainer container;

    /**
     * for automated parsing
     */
    protected TransferableEntity() {
    }

    /**
     * conventional constructor
     *
     * @param container
     */
    public TransferableEntity(TransferableContainer container) {
        this.container = container;
        id = container.register(this);
    }

    public long getId() {
        return id;
    }

    public class JSON_KEYS {
        public static final String type = "type";
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.put(JSON_KEYS.type, getType());
        return result;
    }

    public abstract void updateByJson(JSONObject json, EntityUpdater instaiciator);

    public void instanciateByJson(long id, TransferableContainer container, JSONObject json, EntityUpdater instaiciator) {
        this.id = id;
        this.container=container;                
        updateByJson(json, instaiciator);
    }

    public abstract String getType();

    public TransferableContainer getContainer() {
        return container;
    }

    public void delete() {
        container.unregister(this);
    }
}
