/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model;

import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.space.Space;

/**
 *
 * @author Ivan
 */
public interface ILevelVisitor {
    public void visitSpace(Space space);
    public void visitPlanetSurface(PlanetSurface surface);
}
