package ru.bel_riose.the_new_gaea_client.html;

import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class TheNewGaeaClientHtml extends GwtApplication {
	@Override
	public ApplicationListener getApplicationListener () {
		return new TheNewGaeaClient();
	}
	
	@Override
	public GwtApplicationConfiguration getConfig () {
		return new GwtApplicationConfiguration(480, 320);
	}
}
