/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.map_screen.macro_screen;

import com.badlogic.gdx.Gdx;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.view.MacroDrawer;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;
import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;

/**
 *
 * @author Bel_Riose
 */
public class MacroScreen extends Screen<TheNewGaeaTestDesk, MapCamera2D, MacroScreenInputHandler, MacroScreenGUI> {
    private final MacroDrawer drawer;

    private Level currentLevel;
    
    public MacroScreen(TheNewGaeaTestDesk application) {
        super(application);
        camera2d = new MapCamera2D(0, 0, 1,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        gui = new MacroScreenGUI(this);
        defaultInputHandler= new MacroScreenInputHandler(this);
        drawer= new MacroDrawer();
        
        currentLevel=application.sim.getRoot();
    }    

    @Override
    public void render(float delta) {
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        spriteBatch.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        drawer.draw(currentLevel, shapeRenderer,spriteBatch, camera2d);
        for (GameObject gameObject : currentLevel.getObjects()) {
            drawer.draw(gameObject, shapeRenderer,spriteBatch, camera2d);
        }

        gui.update();
        gui.actNdraw();
    }    

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(Level currentLevel) {
        this.currentLevel = currentLevel;
    }
}
