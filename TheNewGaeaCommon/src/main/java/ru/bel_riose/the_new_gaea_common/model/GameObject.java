/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model;

import ru.bel_riose.the_new_gaea_common.model.transferable.TransferableEntity;
import java.io.Serializable;
import org.json.simple.JSONObject;
import ru.bel_riose.numericalmethods.kinematics2d.Point;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;

class DifferentSimulationException extends RuntimeException{}


/**
 *
 * @author Bel_Riose
 */
public abstract class GameObject extends TransferableEntity implements Point,Serializable{
    private Level level;
    private Vector pos, speed;
    private double angle, angleSpeed;

    
    /**for json parsers
     */
    protected GameObject() {
    }

    /**conventional constructor
     * 
     * @param pos
     * @param angle
     * @param speed
     * @param angleSpeed
     * @param level 
     */
    public GameObject(Vector pos,double angle, Vector speed,double angleSpeed, Level level) {
        super(level.getContainer());
        this.pos = pos;
        this.angle=angle;
        this.speed = speed;
        this.angleSpeed=angleSpeed;
        this.level = level;        
        this.level.getObjects().add(this);        
    }
    
    @Override
    public void setPos(Vector pos) {
        this.pos = pos;
    }
    
    @Override
    public void setSpeed(Vector speed) {
        this.speed = speed;
    }

    @Override
    public Vector getPos() {
        return pos;
    }

    @Override
    public Vector getSpeed() {
        return speed;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getAngleSpeed() {
        return angleSpeed;
    }

    public void setAngleSpeed(double angleSpeed) {
        this.angleSpeed = angleSpeed;
    }
    
    public Level getLevel() {
        return level;
    }

    public final void setLevel(Level level) {
        this.level = level;
        if (getContainer()!=level.getContainer())throw new DifferentSimulationException();
    }
   
    /**
     * проверка на принадлежность точки объекту. Ввел для проверки клика.
     *
     * @param p
     * @return
     */
    public abstract boolean isInside(Vector p);

    public abstract void update(double dt);
    
    @Override
    public void delete(){
        super.delete();
        level.getObjects().remove(this);        
        level.acceptVisitor(new DeleteObjectVisitor(this));
    };
    
    public class JSON_KEYS extends TransferableEntity.JSON_KEYS{
        public static final String pos= "pos",
                speed="speed",angle="angle",angleSpeed="angleSpeed";
    }
    
    @Override
    public JSONObject toJson(){
        JSONObject result = super.toJson();
        result.put(JSON_KEYS.pos, Vector.toJson(pos));        
        result.put(JSON_KEYS.angle, angle);
        result.put(JSON_KEYS.speed, Vector.toJson(speed));
        result.put(JSON_KEYS.angleSpeed, angleSpeed);
        return result;    
    };

    @Override
    public void updateByJson(JSONObject json,EntityUpdater instanciator){
        //super.updateByJson(json);
        pos=Vector.fromJson((JSONObject) json.get(JSON_KEYS.pos));
        speed=Vector.fromJson((JSONObject) json.get(JSON_KEYS.speed));
        angle=(double)json.get(JSON_KEYS.angle);
        angleSpeed=(double)json.get(JSON_KEYS.angleSpeed);
    };
    
    
    
    public abstract void acceptVisitor(IGameObjectVisitor visitor);

    static class DeleteObjectVisitor implements ILevelVisitor{
        GameObject object;

        public DeleteObjectVisitor(GameObject object) {
            this.object = object;
        }
                
        @Override
        public void visitSpace(Space space) {
            if(object==space.getCenter()){
                space.setCenter(null);
            }
        }

        @Override
        public void visitPlanetSurface(PlanetSurface surface) {
        }
        
    }
}
