package ru.bel_riose.the_new_gaea_client.android;

import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class TheNewGaeaClientActivity extends AndroidApplication {

	@Override
	public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
			config.useGL20 = true;
			initialize(new TheNewGaeaClient(), config);
	}
}
