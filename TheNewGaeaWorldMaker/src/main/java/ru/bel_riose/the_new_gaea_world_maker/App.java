package ru.bel_riose.the_new_gaea_world_maker;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import org.json.simple.JSONArray;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.*;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws IOException {
        /* //Система в большом масштабе(не работала)
        GameWorld system1 = new GameWorld(6.67384e-13, 1000);
        GameObject zun=new DiskCelestialObject("Zun", 6955100, 1.9891e+28, 0, 0, 0, 0, 0, 0, system1);
        system1.Children.add(zun);
        GameObject gaea= new DiskCelestialObject("Gaea", 63710, 5.9736e+22, 0, 297.83, 0, 1495982610, 0, 0, system1);
        system1.Children.add(gaea);
        GameObject zeus=new DiskCelestialObject("Zeus", 699110, 1.8986e+26, 0, -130.7, 0, -7785472e+3, 0, 0, system1);
        system1.Children.add(zeus);
        
        system1.setCenter(zun);
        */
        
        
         //Небольшая система
        Simulation simulation1 = new Simulation();
        Space system1 = new Space(1, 4, new ODEWrapSolver(new RungeKutta4()),simulation1);
        simulation1.setRoot(system1);
        GameObject zun=new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10,system1);        
        
        PlanetSurface gaeaSurface=new PlanetSurface(simulation1, 1, 0.001);
        
        GameObject gaea=new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, system1,gaeaSurface);        
        GameObject zeus=new Planet("Zeus", 10, 1, -0.11, 0, 0, 0, -1000, 0, system1,null);
        GameObject gefest=new Planet("Gefest", 0.5, 0.05, 0.1, 0, 0, 0, -1020, 0, system1,null);
        
        Character char1=new Character(0, 1.00001, Math.PI/6,0.0001, gaeaSurface);
        Character char2=new Character(0.003, 1.00001, 0,0.0001, gaeaSurface);
        Character char3=new Character(0.006, 1.00001, 0,0.0001, gaeaSurface);
        
        system1.setCenter(zun);  
        
        
        /* //кеплеровы орбиты 3 тел
        GameWorld system1 = new GameWorld(400, 4,GameWorld.Methods.RUNGECUTTA);
        GameObject o1=new DiskCelestialObject("o1", 10, 1, 0, -0.6, 0, 500, 0, 0, system1);
        GameObject o2=new DiskCelestialObject("o2", 10, 1, 0.6*Math.cos(Math.PI*2.0/3.0-Math.PI/2), 0.6*Math.sin(Math.PI*2.0/3.0-Math.PI/2), 0, 500*Math.cos(Math.PI*2.0/3.0), 500*Math.sin(Math.PI*2.0/3.0), 0, system1);
        GameObject o3=new DiskCelestialObject("o3", 10, 1, -0.6*Math.cos(Math.PI*2.0/3.0-Math.PI/2), 0.6*Math.sin(Math.PI*2.0/3.0-Math.PI/2), 0, 500*Math.cos(Math.PI*2.0/3.0), -500*Math.sin(Math.PI*2.0/3.0), 0, system1);
        
        system1.Children.add(o1);
        system1.Children.add(o2);
        system1.Children.add(o3);
        
        
        GameObject c=new DiskCelestialObject("c", 1, 0, 0, 0, 0, 0,0, 0, system1);
        system1.Children.add(c);
        system1.setCenter(c);*/
        
       /* //2 тела-тест
        GameWorld system1 = new GameWorld(1, 4, GameWorld.Methods.RUNGECUTTA);
        GameObject zun=new DiskCelestialObject("Zun", 100, 10, 0, 0, 0, 0, 0, 0, system1);
        system1.Children.add(zun);
        GameObject gaea=new DiskCelestialObject("Gaea", 1, 0, 0, 0.13, 0, 400, 0, 0, system1);
        system1.Children.add(gaea);
        system1.setCenter(zun);
        */

        
        
        String simFilename = "../system1.ser";
        simulation1.SaveToFile(simFilename); 
        System.out.println("World saved to" +simFilename);
                
        Profile aProfile= new Profile("A", "");
        Profile bProfile= new Profile("B", "");
        Profile johnProfile = new Profile("John","Carter");
        aProfile.setCurrentCharacter(char1);
        bProfile.setCurrentCharacter(char2);
        johnProfile.setCurrentCharacter(char3);
        
        String profilesFilename="../profiles1.json";
        JSONArray profilesJson = new JSONArray();
        profilesJson.add(Profile.toJson(aProfile));
        profilesJson.add(Profile.toJson(bProfile));
        profilesJson.add(Profile.toJson(johnProfile));
        
        FileOutputStream fos = new FileOutputStream(profilesFilename);
        PrintWriter pw = new PrintWriter(fos);
        pw.print(profilesJson.toString());
        pw.close();        
    }
}
