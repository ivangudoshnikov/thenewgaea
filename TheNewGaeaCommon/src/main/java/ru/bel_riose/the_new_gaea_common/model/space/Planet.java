/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.space;

import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;

/**
 *
 * @author Bel_Riose
 */
public class Planet extends DiskCelestialObject {

    PlanetSurface surface;

    /**
     * for json parsers
     */
    public Planet() {
    }

    /**
     * Conventional constructor 1
     *
     * @param name
     * @param pos
     * @param angle
     * @param speed
     * @param angleSpeed
     * @param r
     * @param m
     * @param level
     */
    public Planet(String name, Vector pos, double angle, Vector speed, double angleSpeed, double r, double m, Level level, PlanetSurface surface) {
        super(name, pos, angle, speed, angleSpeed, r, m, level);
        this.surface = surface;
        if (surface!=null){
            surface.setPlanet(this);
        }
    }

    /**
     * Conventional constructor 2
     *
     * @param name
     * @param r
     * @param m
     * @param vx
     * @param vy
     * @param angleV
     * @param x
     * @param y
     * @param angle
     * @param level
     */
    public Planet(String name, double r, double m, double vx, double vy, double angleV, double x, double y, double angle, Level level, PlanetSurface surface) {
        this(name, new Vector(x, y), angle, new Vector(vx, vy), angleV, r, m, level, surface);
    }

    @Override
    public void update(double dt) {
    }

    public static final String TYPE_NAME = "Planet";

    public PlanetSurface getSurface() {
        return surface;
    }
    
    @Override
    public String getType() {
        return TYPE_NAME;
    }

    @Override
    public void acceptVisitor(IGameObjectVisitor visitor) {
        visitor.visitPlanet(this);
    }

    public class JSON_KEYS extends DiskCelestialObject.JSON_KEYS {

        public static final String surfaceId = "surfaceId";
    }

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instanciator) {
        super.updateByJson(json, instanciator);
        Object surfaceId = json.get(JSON_KEYS.surfaceId);
        if (surfaceId == null) {
            surface = null;
        } else {
            surface = (PlanetSurface) instanciator.getUpdated((Long) surfaceId);
            surface.setPlanet(this);
        }
    }

    @Override
    public JSONObject toJson() {
        JSONObject result = super.toJson();
        if (surface == null) {
            result.put(JSON_KEYS.surfaceId, null);
        } else {
            result.put(JSON_KEYS.surfaceId, surface.getId());
        }
        return result;
    }

}
