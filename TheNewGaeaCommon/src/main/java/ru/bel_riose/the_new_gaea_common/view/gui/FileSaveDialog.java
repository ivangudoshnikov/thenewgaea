/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.view.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import java.io.File;
import java.util.LinkedList;
import java.util.logging.Logger;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Bel_Riose
 */
public class FileSaveDialog {
//TODO: СДЕЛАТЬ НОРМАЛЬНО!! 
//ОДНО ОКНО, ОДИН СПИСОК, ПРОСТО ОБНОВЛЯЕТСЯ
//И ВСЕ LISTENER'Ы В ОДНО МЕСТО!
    private static final Logger log = Logger.getLogger(FileSelectDialog.class.getName());

    public interface FileHandler {

        public void handle(File file);
    }
    private GraphicalUserInterface gui;
    private FileSelectDialog.FileHandler handler;
    private boolean onRoot;
    private List list;
    private File currentPath;
    private TextField txtFilename;
    private LinkedList<File> filesList, foldersList;

    private class OpenButtonListener extends ClickListener {

        private Window window;

        public OpenButtonListener(int button, Window window) {
            super(button);
            this.window = window;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            int selected = list.getSelectedIndex();
            if (selected != -1) {
                window.remove();
                if (currentPath != null) {
                    //если выбираем среди папок а не дисков

                    if (selected == 0) {
                        if (onRoot) {
                            currentPath = null;
                            drawList();
                        } else {
                            currentPath = currentPath.getParentFile();
                            drawList();
                        }
                        return;
                    } else {
                        selected--;
                    }


                    if (selected < foldersList.size()) {
                        currentPath = foldersList.get(selected);
                        drawList();
                    } else {
                        handler.handle(filesList.get(selected - foldersList.size()));
                    }
                } else {
                    currentPath = File.listRoots()[selected];
                    drawList();
                }
            }else{
                if ((currentPath!=null)&&(txtFilename.getText()!="")&&(txtFilename.getText()!="..")){
                    window.remove();
                    handler.handle(new File(currentPath, txtFilename.getText()));
                }
            }
        }
    }

    public FileSaveDialog(GraphicalUserInterface gui, FileSelectDialog.FileHandler handler) {
        this.gui = gui;
        currentPath = new File(".");
        currentPath = currentPath.getAbsoluteFile().getParentFile();
        onRoot = (currentPath.getParentFile() == null);
        this.handler = handler;
        drawList();
    }

    private void drawList() {
        final Window winSelectFile = new Window("Select file", gui.getSkin());
        final Stage stage=gui.getStage();
        stage.addActor(winSelectFile);
        winSelectFile.setModal(true);
        Label lblPath = new Label("", gui.getSkin());
        winSelectFile.add(lblPath);
        winSelectFile.row();

        final String[] names;
        if (currentPath != null) {
            //Если мы не выбираем среди дисков
            onRoot = (currentPath.getParentFile() == null);
            currentPath = currentPath.getAbsoluteFile();
            lblPath.setText(currentPath.getAbsolutePath());
            File[] fullList = currentPath.listFiles();
            if (fullList == null) {
                winSelectFile.remove();
                new Message("Error", "Access denied", stage, gui.getSkin());
                return;
            }
            filesList = new LinkedList<File>();
            foldersList = new LinkedList<File>();
            //Отделяем папки от файлов.
            for (File f : fullList) {
                if (f.isDirectory()) {
                    foldersList.add(f);
                } else {
                    filesList.add(f);
                }
            }
            names = new String[fullList.length + 1];

            //Добавляем папку верхнего уровня
            names[0] = "..";
            int i = 1;

            for (File f : foldersList) {
                names[i] = f.getName();
                i++;
            }
            for (File f : filesList) {
                names[i] = f.getName();
                i++;
            }
        } else {
            lblPath.setText("Filesystems:");
            //выбор диска
            File[] drivesList = File.listRoots();
            names = new String[drivesList.length];
            for (int i = 0; i < drivesList.length; i++) {
                names[i] = drivesList[i].getAbsolutePath();
            }
        }
        list = new List(names, gui.getSkin());


        list.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int selected = list.getSelectedIndex();
                if ((currentPath != null) && (selected > foldersList.size())) {
                    txtFilename.setText(names[selected]);
                }else{
                    txtFilename.setText("");
                }
            }
        });

        ScrollPane scrollPane = new ScrollPane(list, gui.getSkin());
        winSelectFile.add(scrollPane);
        winSelectFile.row();

        txtFilename = new TextField("", gui.getSkin());
        txtFilename.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                list.setSelectedIndex(-1);
            }
        });
        winSelectFile.add(txtFilename);
        winSelectFile.row();

        TextButton btnCreateFolder = new TextButton("Create folder", gui.getSkin());
        btnCreateFolder.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                boolean result =false;
                if (currentPath!=null){
                    File folder= new File(currentPath,txtFilename.getText());
                    result=folder.mkdir();
                    winSelectFile.remove();
                    drawList();
                }
                if (!result) new Message("Error", "Can't create folder", stage, gui.getSkin());
            }
        });

        TextButton btnCancel = new TextButton("Cancel", gui.getSkin());
        btnCancel.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                winSelectFile.remove();
            }
        });

        TextButton btnOpen = new TextButton("Select", gui.getSkin());
        btnOpen.addListener(new FileSaveDialog.OpenButtonListener(Input.Buttons.LEFT, winSelectFile));

        Table tblButtons = new Table(gui.getSkin());
        tblButtons.add(btnCreateFolder);
        tblButtons.add(btnCancel);
        tblButtons.add(btnOpen);

        winSelectFile.add(tblButtons);
        winSelectFile.pack();
        //поправляю размеры, не знаю как лучше
        if (winSelectFile.getHeight() > stage.getHeight()) {
            winSelectFile.setHeight(stage.getHeight());
        }
        if (winSelectFile.getWidth() > stage.getWidth()) {
            winSelectFile.setWidth(stage.getWidth());
        }

        winSelectFile.setPosition((stage.getWidth() - winSelectFile.getWidth()) / 2, (stage.getHeight() - winSelectFile.getHeight()) / 2);
    }
}
