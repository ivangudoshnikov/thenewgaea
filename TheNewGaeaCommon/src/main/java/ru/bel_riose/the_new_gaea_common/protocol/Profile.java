package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;

/**
 * Профиль на сервере
 *
 * @author Ivan
 */
public class Profile {

    private String name, password;
    private Character currentCharacter;
    private Connection connection;
    
    /**
     * Conventional constructor
     *
     * @param name
     * @param password
     */
    public Profile(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Character getCurrentCharacter() {
        return currentCharacter;
    }

    public void setCurrentCharacter(Character currentCharacter) {
        this.currentCharacter = currentCharacter;
    }

    public static class JSON_KEYS {
        public static final String name = "name", password = "password", characterId = "characterId";
    }

    public static JSONObject toJson(Profile profile) {
        JSONObject result = new JSONObject();
        result.put(JSON_KEYS.name, profile.name);
        result.put(JSON_KEYS.password, profile.password);
        result.put(JSON_KEYS.characterId, profile.getCurrentCharacter().getId());
        return result;
    }
    
    public static Profile fromJson(JSONObject json, Simulation sim){
        Profile result= new Profile((String)json.get(JSON_KEYS.name), (String)json.get(JSON_KEYS.password));
        result.setCurrentCharacter((Character) sim.getEntity((Long)json.get(JSON_KEYS.characterId)));
        return result;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
    
}
