/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core;

import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.protocol.AbstractMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;
import ru.bel_riose.the_new_gaea_common.protocol.CurrentCharacterMessageBuilder;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.protocol.LoginMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.LoginRejectMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.LoginSuccessMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.SimUpdateMessageBuilder;

class UnknownMessageTypeException extends RuntimeException {
}

/**
 * Client message processor
 *
 * @author Ivan
 */
public class MessageManager {

    private static final LoginMessageBuilder loginMessageBuilder = new LoginMessageBuilder();

    public static JSONObject getLoginMessage(Profile profile) {
        return loginMessageBuilder.toJson(profile);
    }

    static void processMessage(JSONObject message, TheNewGaeaClient application) {
        String type = (String) message.get(AbstractMessageBuilder.JSON_KEYS.type);
        long characterId;
        switch (type) {
            //����� ������������ ���������
            case CurrentCharacterMessageBuilder.TYPE_NAME:
                characterId = (Long) message.get(CurrentCharacterMessageBuilder.JSON_KEYS.currentCharacterId);
                application.getProfile().setCurrentCharacter((Character) application.getSim().getEntity(characterId));
                break;
            //�������� �����
            case LoginSuccessMessageBuilder.TYPE_NAME:
                application.getSim().updateByJson((JSONObject) message.get(LoginSuccessMessageBuilder.JSON_KEYS.sim));
                characterId = (Long) message.get(LoginSuccessMessageBuilder.JSON_KEYS.currentCharacterId);
                application.getProfile().setCurrentCharacter((Character) application.getSim().getEntity(characterId));
                //shoud be connectionScreen, switch to macroScreen
                application.switchScreen();
                break;
            //�������� �����
            case LoginRejectMessageBuilder.TYPE_NAME:
                application.connectionScreen.showLoginRejectedMessage((String) message.get(LoginRejectMessageBuilder.JSON_KEYS.reason));
                break;
            //���������� ����
            case SimUpdateMessageBuilder.TYPE_NAME:
                application.getSim().updateByJson((JSONObject) message.get(SimUpdateMessageBuilder.JSON_KEYS.sim));
                break;
            default:
                throw new UnknownMessageTypeException();
        }
    }
}
