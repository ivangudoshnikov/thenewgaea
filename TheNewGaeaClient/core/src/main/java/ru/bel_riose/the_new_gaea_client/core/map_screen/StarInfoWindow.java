package ru.bel_riose.the_new_gaea_client.core.map_screen;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.the_new_gaea_client.core.map_screen.InfoWindowFactory.InfoWindow;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Bel_Riose
 */
class StarInfoWindow extends InfoWindow<Star> {

    Label lblName, lblM, lblR, lblLuminocity;

    public StarInfoWindow(Star star, final GraphicalUserInterface gui) {
        super(star, "Star info", gui);

        add(new Label("Name: ", gui.getSkin()));
        lblName = new Label(star.getName(), gui.getSkin());
        add(lblName);
        row();

        add(new Label("Mass: ", gui.getSkin()));
        lblM = new Label(String.valueOf(star.getM()), gui.getSkin());
        add(lblM);
        row();

        add(new Label("Radius: ", gui.getSkin()));
        lblR = new Label(String.valueOf(star.getR()), gui.getSkin());
        add(lblR);
        row();

        add(new Label("Luminocity: ", gui.getSkin()));
        lblLuminocity = new Label(String.valueOf(star.getLuminocity()), gui.getSkin());
        add(lblLuminocity);
    }

    @Override
    public void update(Simulation sim) {
        lblName.setText(object.getName());
        lblM.setText(String.valueOf(object.getM()));
        lblR.setText(String.valueOf(object.getR()));
        lblLuminocity.setText(String.valueOf(object.getLuminocity()));
    }
}