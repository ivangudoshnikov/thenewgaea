/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core.map_screen;

import com.badlogic.gdx.scenes.scene2d.ui.Window;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

class InfoWindowFactory {
    public static abstract class InfoWindow<T extends GameObject> extends Window {

        protected T object;

        public InfoWindow(T object, String title, GraphicalUserInterface gui) {
            super(title, gui.getSkin());
            this.object = object;
        }

        public abstract void update(Simulation sim);
    }
    
    private GraphicalUserInterface gui;
    private InfoWindow result;
    private final InfoWindowFactoryImpl impl= new InfoWindowFactoryImpl();
    
    public InfoWindow getInfoWindow(GameObject gameObject, GraphicalUserInterface gui){
        this.gui=gui;
        gameObject.acceptVisitor(impl);
        return result;
    };
    
    private class InfoWindowFactoryImpl implements IGameObjectVisitor{
        @Override
        public void visitPlanet(Planet planet) {
           result=new PlanetInfoWindow(planet,gui);
        }

        @Override
        public void visitStar(Star star) {
            result=new StarInfoWindow(star, gui);
        }

        @Override
        public void visitCharacter(Character character) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }        
    } 
}