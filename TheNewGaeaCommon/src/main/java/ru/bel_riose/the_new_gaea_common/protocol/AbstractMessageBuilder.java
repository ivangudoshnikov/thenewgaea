/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;

/**
 *
 * @author Ivan
 */
public abstract class AbstractMessageBuilder {
    public abstract String getType();
    
    public class JSON_KEYS{
        public static final String type = "type";
    }
    
    protected JSONObject toJson(){
        JSONObject result =new JSONObject();
        result.put(JSON_KEYS.type, getType());
        return result;   
    }   
}
