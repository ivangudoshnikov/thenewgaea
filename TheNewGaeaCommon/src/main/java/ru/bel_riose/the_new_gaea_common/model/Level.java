package ru.bel_riose.the_new_gaea_common.model;

import java.io.Serializable;
import java.util.LinkedList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;
import ru.bel_riose.the_new_gaea_common.model.transferable.TransferableEntity;

/**
 *
 * @author Ivan
 */
public abstract class Level extends TransferableEntity implements Serializable {

    private LinkedList<GameObject> objects = new LinkedList<>();
    
    /**
     * for json parsers
     */
    protected Level() {
    }
    
    /**Conventional constructor
     * 
     * @param simulation 
     */
    public Level(Simulation simulation) {
        super(simulation);
    }

    public abstract void update(double dt);

    public LinkedList<GameObject> getObjects() {
        return objects;
    }

    public void delete() {
        super.delete();
        for (GameObject gameObject : objects) {
            gameObject.delete();
        }
    }

    public class JSON_KEYS extends TransferableEntity.JSON_KEYS {

        public static final String objectsIds = "objectsIds";
    }

    @Override
    public JSONObject toJson() {
        JSONObject result = super.toJson();
        JSONArray objectsIds = new JSONArray();
        for (GameObject gameObject : objects) {
            objectsIds.add(gameObject.getId());
        }
        result.put(JSON_KEYS.objectsIds, objectsIds);

        return result;
    }

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instanciator) {
        //super.updateByJson(json);
        JSONArray objectsIds = (JSONArray) json.get(JSON_KEYS.objectsIds);
        objects.clear();
        for (Object objId : objectsIds) {
            GameObject obj=(GameObject) instanciator.getUpdated((Long)objId);
            objects.add(obj); //добавляем обновленный объект в список
            obj.setLevel(this); //указываем  объекту что он находится в данном уровне
        }
    }
    
    public abstract boolean isOutside(Vector v);

    public abstract void acceptVisitor(ILevelVisitor visitor);
}
