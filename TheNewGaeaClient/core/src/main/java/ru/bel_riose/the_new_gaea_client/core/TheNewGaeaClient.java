package ru.bel_riose.the_new_gaea_client.core;

import ru.bel_riose.the_new_gaea_client.core.connection_screen.ConnectionScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import ru.bel_riose.the_new_gaea_client.core.macro_screen.MacroScreen;
import ru.bel_riose.the_new_gaea_client.core.map_screen.MapScreen;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.protocol.Connection;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

public class TheNewGaeaClient extends Game {

    private Profile profile;

    float elapsed;
    private Simulation sim;
    private Connection connection;
    private JSONParser jsonParser;
    ConnectionScreen connectionScreen;
    MapScreen mapScreen;
    MacroScreen macroScreen;   

    public Exception connect(String address, int port) {
        InetAddress ipAddress;
        Exception result = null;
        try {
            ipAddress = InetAddress.getByName(address);
            connection = new Connection(new Socket(ipAddress, port));

        } catch (UnknownHostException ex) {
            Logger.getLogger(TheNewGaeaClient.class.getName()).log(Level.SEVERE, null, ex);
            result = ex;
        } catch (IOException ex) {
            Logger.getLogger(TheNewGaeaClient.class.getName()).log(Level.SEVERE, null, ex);
            result = ex;
        }
        return result;
    }

    public void login() {
        try {
            connection.write(MessageManager.getLoginMessage(profile).toString());
        } catch (IOException ex) {
            Logger.getLogger(TheNewGaeaClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setupProfile(String name, String password) {
        profile = new Profile(name, password);
    }

    @Override
    public void create() {
        sim = new Simulation();
        jsonParser = new JSONParser();
        connectionScreen = new ConnectionScreen(this);
        mapScreen = new MapScreen(this);
        macroScreen= new MacroScreen(this);
        this.setScreen(connectionScreen);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    /**
     * Main loop
     */
    @Override
    public void render() {
        if (connection != null) {
            receiveMessage();
        };
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        super.render();
    }

    void receiveMessage() {
        try {
            while (connection.available() != 0) {
                JSONObject json = (JSONObject) jsonParser.parse(connection.read());
                MessageManager.processMessage(json, this);
            }
        } catch (Throwable ex) {
            Logger.getLogger(TheNewGaeaClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public Simulation getSim() {
        return sim;
    }

    public Profile getProfile() {
        return profile;
    }
    
    public void switchScreen(){
        //ConnectionScreen => MacroScreen
        //MacroScreen => MapScreen
        //MapScreen =>MacroScreen
        
        if(getScreen()==macroScreen){
            setScreen(mapScreen);
        }else if(getScreen()==mapScreen){
            setScreen(macroScreen);
        }else if(getScreen()==connectionScreen){
            setScreen(macroScreen);
        }
    }
}
