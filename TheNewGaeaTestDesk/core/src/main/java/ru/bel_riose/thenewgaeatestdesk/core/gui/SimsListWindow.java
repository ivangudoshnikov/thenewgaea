/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.gui;

import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenGUI;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Logger;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreen;
import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;
import ru.bel_riose.thenewgaeatestdesk.core.TrajectoriesMap;

/**
 *
 * @author Ivan
 */
public class SimsListWindow extends Window {

    private static final Logger log = Logger.getLogger(SimsListWindow.class.getName());

    static class SimState extends Table {

        private Simulation sim;
        private TrajectoriesMap trajectories;

        public SimState(final SimsListWindow window, Simulation sim, TrajectoriesMap trajectories, Skin skin, String caption) {
            super(skin);
            if (trajectories != null) {
                this.trajectories = (TrajectoriesMap) trajectories.clone();
            }
            this.sim = (Simulation) sim.clone();
            TextField txtCaption = new TextField(caption, skin);
            
            TextButton btnLoad = new TextButton("Load", skin);
            btnLoad.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    window.mapScreen.getApplication().setSimRunning(false);
                    window.mapScreen.getGui().setSelectedObject(null);
                    window.mapScreen.getApplication().sim = (Simulation) SimState.this.sim.clone();
                    if (SimState.this.trajectories != null) {
                        window.mapScreen.getApplication().trajectories = (TrajectoriesMap) SimState.this.trajectories.clone();
                    }
                    window.mapScreen.getGui().update();                    
                }
            });
            txtCaption.setWidth(btnLoad.getWidth());
            TextButton btnDelete = new TextButton("Del", skin);
            btnDelete.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    window.simsList.remove(SimState.this);
                    window.update();
                }
            });

            this.add(txtCaption);
            this.row();
            Table tblButtons = new Table(skin);
            tblButtons.add(btnLoad);
            tblButtons.add(btnDelete);
            tblButtons.add(new Label("  ", skin));
            this.add(tblButtons);            
            this.padLeft(1);
            this.padRight(1);
        }
    }
    private MapScreen mapScreen;
    private Table scrollContainer, buttonContainer;
    private MapScreenGUI ui;
    private LinkedList<SimState> simsList = new LinkedList<SimState>();
    private final static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd MMM YYYY");

    public SimsListWindow(MapScreen mapScreen,MapScreenGUI gui) {
        super("Events", gui.getSkin());
        this.mapScreen = mapScreen;
        this.ui = gui;

        
        scrollContainer = new Table(this.ui.getSkin());

        //������ ������� ��� ������
        buttonContainer = new Table(this.ui.getSkin());
        Button btnAdd = new Button(this.ui.getSkin());
        btnAdd.add(new Label("   +   ", this.ui.getSkin()));
        btnAdd.row();
        btnAdd.add(new Label("     ", this.ui.getSkin()));
        btnAdd.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                addSimState(sdf.format(new Date()), SimsListWindow.this.mapScreen.getApplication().sim, SimsListWindow.this.mapScreen.getApplication().trajectories);
            }
        });
        buttonContainer.add(btnAdd);
        Button btnClear = new TextButton("Clear", this.ui.getSkin());
        btnClear.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                simsList.clear();
                update();
            }
        });
        buttonContainer.row();
        buttonContainer.add(btnClear);        
        buttonContainer.row();
        buttonContainer.add(new Label(" ", gui.getSkin()));
        
        ScrollPane scrollPane=new ScrollPane(scrollContainer, this.ui.getSkin());
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollingDisabled(false, true);
        this.add(scrollPane);
        update();
        gui.getStage().addActor(this);
        this.setPosition(gui.getStage().getWidth() / 2, 0);
        this.setMovable(false);

    }

    public void addSimState(String caption, Simulation sim, TrajectoriesMap trajectories) {
        simsList.addFirst(new SimState(this, sim, trajectories, ui.getSkin(), caption));
        update();
    }

    final void update() {
        scrollContainer.clear();
        scrollContainer.add(buttonContainer);
        for (SimState simState : simsList) {
            scrollContainer.add(simState);
        }
        this.pack();
        if (this.getWidth() > ui.getStage().getWidth() / 2) {
            this.setWidth(ui.getStage().getWidth() / 2);            
        }
        
    }
}
