package ru.bel_riose.the_new_gaea_client.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import java.awt.Dimension;
import java.awt.Toolkit;

import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;



public class TheNewGaeaClientDesktop {
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL20 = true;
                config.fullscreen=false;
                config.width=600;
                config.height=900;
//                config.fullscreen=true;
//                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//                config.width=screenSize.width;
//                config.height=screenSize.height;
                new LwjglApplication(new TheNewGaeaClient(), config);
	}
}
