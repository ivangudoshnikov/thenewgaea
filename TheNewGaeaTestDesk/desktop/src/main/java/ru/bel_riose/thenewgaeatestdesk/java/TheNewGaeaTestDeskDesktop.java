package ru.bel_riose.thenewgaeatestdesk.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import java.awt.Dimension;
import java.awt.Toolkit;

import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;

public class TheNewGaeaTestDeskDesktop {
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL20 = true;                
                config.fullscreen=true;
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                config.width=screenSize.width;
                config.height=screenSize.height;
//                config.width=1600;
//                config.height=900;
		new LwjglApplication(new TheNewGaeaTestDesk(), config);
	}
}
