/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.libgdx_2d_skeleton.Camera2D;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;

/**
 *
 * @author Ivan
 */
public class MapDrawer {

    private final DrawerImpl impl = new DrawerImpl();
    private ShapeRenderer shapeRenderer;
    private Camera2D camera;

    /**
     * @param gameObject
     * @param shapeRenderer tool
     * @param camera 
     */
    public void draw(GameObject gameObject, ShapeRenderer shapeRenderer, Camera2D camera) {
        this.shapeRenderer = shapeRenderer;
        this.camera = camera;
        
        Matrix4 modelMatrix = new Matrix4().translate((float) gameObject.getPos().x, (float) gameObject.getPos().y, 0);
        modelMatrix.rotate(0, 0, 1, -(float) Math.toDegrees(gameObject.getAngle()));
        Matrix4 modelViewProjMatrix = camera.getViewProjectionMatrix().mul(modelMatrix);
        
        //this.spriteBatch.setProjectionMatrix(modelViewProjMatrix);
        this.shapeRenderer.setProjectionMatrix(modelViewProjMatrix);
        gameObject.acceptVisitor(impl);
        //this.spriteBatch.setProjectionMatrix(camera.getCombined());
        this.shapeRenderer.setProjectionMatrix(camera.getViewProjectionMatrix());
    }

    private class DrawerImpl implements IGameObjectVisitor {

        @Override
        public void visitPlanet(Planet planet) {
            Vector coord = planet.getPos();
            shapeRenderer.setColor(0.5f, 0.2f, 0, 1);
            if (camera.rescaleMathToScr(planet.getR()) > 1) {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(0, 0, (float) planet.getR(), 20);
                shapeRenderer.end();
            } else {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
                shapeRenderer.point(0, 0, 0);
                shapeRenderer.end();
            }
        }

        @Override
        public void visitStar(Star star) {
            Vector coord = star.getPos();
            double r = star.getR();
            shapeRenderer.setColor(1, 1, 0, 1);
            if (camera.rescaleMathToScr(r + star.getLuminocity()) > 1) {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(0, 0, (float) (r + star.getLuminocity()), 20);
                shapeRenderer.setColor(1, 0.8f, 0, 1);
                shapeRenderer.circle(0, 0, (float) r, 20);
                shapeRenderer.end();
            } else {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
                shapeRenderer.point(0, 0, 0);
                shapeRenderer.end();
            }
        }

        @Override
        public void visitCharacter(Character character) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
