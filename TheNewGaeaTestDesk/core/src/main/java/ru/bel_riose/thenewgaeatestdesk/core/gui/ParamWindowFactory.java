/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.gui;

import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenGUI;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;

/**
 *
 * @author Bel_Riose
 */
public class ParamWindowFactory {

    /**
     * @author Bel_Riose ���� ��� �������������� �������, ������ ��� ���������!
     */
    static public abstract class ParamWindow<T extends GameObject> extends Window {

        protected T object;

        public ParamWindow(T object, String title, MapScreenGUI gui) {
            super(title, gui.getSkin());
            this.object = object;
        }

        public abstract void update();
    }
    private MapScreenGUI ui;
    private ParamWindow result;
        
    public ParamWindow getParamWindow(GameObject gameObject, MapScreenGUI ui) {
        this.ui = ui;
        gameObject.acceptVisitor(impl);
        return result;
    }
    
    private final ParamWindowFactoryImpl impl= new ParamWindowFactoryImpl();
    private class ParamWindowFactoryImpl implements IGameObjectVisitor{

        @Override
        public void visitPlanet(Planet planet) {
            result=new PlanetParamWindow(planet, ui);
        }

        @Override
        public void visitStar(Star star) {
            result=new StarParamWindow(star, ui);
        }

        @Override
        public void visitCharacter(Character character) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    
    }
}
