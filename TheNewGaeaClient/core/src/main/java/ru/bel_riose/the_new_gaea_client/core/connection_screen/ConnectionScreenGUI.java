/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.the_new_gaea_client.core.connection_screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.bel_riose.the_new_gaea_common.view.gui.Message;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Bel_Riose
 */
class ConnectionScreenGUI extends GraphicalUserInterface<ConnectionScreen> {

    private class AddressWindow extends Window {

        private TextField txtAddress, txtPort;

        public AddressWindow() {
            super("Connection setup", getSkin());
            setModal(true);
            add(new Label("Address", getSkin()));
            add(new Label("Port", getSkin()));
            row();
            txtAddress = new TextField("127.0.0.1", getSkin());
            txtPort = new TextField("2100", getSkin());
            add(txtAddress);
            add(txtPort);
            row();

            TextButton btnConnect = new TextButton("Connect", getSkin());
            btnConnect.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    int port;
                    try {
                        port = Integer.valueOf(txtPort.getText());
                        Exception result = screen.getApplication().connect(txtAddress.getText(), port);
                        if (result != null) {
                            new Message("Error", "Can't connect: " + result.toString(), stage, getSkin());
                        } else {
                            //Network connection established!
                            addressWindow.remove();                            
                            stage.addActor(loginWindow);
                            }
                    } catch (Exception ex) {
                        new Message("Error", "Bad address", stage, getSkin());
                    }

                }
            });
            add(btnConnect);
            pack();
            setPosition((stage.getWidth() - getWidth()) / 2, (stage.getHeight() - getHeight()) / 2);
        }
    }

    private class LoginWindow extends Window {
        private TextField txtLogin, txtPassword;
        
        public LoginWindow() {
            super("Login", getSkin());
            setModal(true);
            add(new Label("Login: ", getSkin()));
            txtLogin = new TextField("", getSkin());
            add(txtLogin);
            row();
            
            add(new Label("Password: ", getSkin()));
            txtPassword = new TextField("", getSkin());
            add(txtPassword);
            row();
            
            TextButton btnLogin = new TextButton("Login", getSkin());
            btnLogin.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    screen.getApplication().setupProfile(txtLogin.getText(), txtPassword.getText());
                    screen.getApplication().login();
                }
            });
            add(btnLogin);
            pack();
            setPosition((stage.getWidth() - getWidth()) / 2, (stage.getHeight() - getHeight()) / 2);
        }
    }
    

    private final AddressWindow addressWindow;
    private final LoginWindow loginWindow;
    
    public ConnectionScreenGUI(ConnectionScreen connectionScreen) {
        super(connectionScreen);
        addressWindow= new AddressWindow();
        loginWindow=new LoginWindow();
        stage.addActor(addressWindow);
    }

    @Override
    public void update() {
    }
    
    void showLoginRejectedMessage(String reason){
        new Message("Error", "Login rejected. \r\n Reason: "+reason, stage, getSkin());
    }

}
