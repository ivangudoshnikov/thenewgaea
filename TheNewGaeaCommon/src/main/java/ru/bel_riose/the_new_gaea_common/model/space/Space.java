/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.space;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.numericalmethods.kinematics2d.IGetAccelerationCallback;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.numericalmethods.kinematics2d.PhysicsSolver;
import ru.bel_riose.numericalmethods.ode.Euler;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.ILevelVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;

/**
 *
 * @author Bel_Riose
 */
public class Space extends Level implements IGetAccelerationCallback {

    static final Logger log = Logger.getLogger("GameWorld");
    private double G, timeSpeed;
    private GameObject center = null;
    private PhysicsSolver physicsSolver;
    private final GravityCollector gravityCollector = new GravityCollector();

    public void setPhysics(PhysicsSolver physicsSolver) {
        this.physicsSolver = physicsSolver;
        getContainer().registerUpdate(this);
    }

    public void setCenter(GameObject center) {
        this.center = center;
        centerization();
        getContainer().registerUpdate(this);
    }

    public GameObject getCenter() {
        return center;
    }

    public double getG() {
        return G;
    }

    public void setG(double G) {
        this.G = G;
        getContainer().registerUpdate(this);
    }

    public double getTimeSpeed() {
        return timeSpeed;
    }

    public void setTimeSpeed(double timeSpeed) {
        this.timeSpeed = timeSpeed;
        getContainer().registerUpdate(this);
    }

    public String getPhysicsSolverName() {
        return physicsSolver.getName();
    }

    /**
     * for json parsers
     */
    public Space() {
    }

   
    /**conventional constructor
     * 
     * @param G
     * @param timeSpeed
     * @param physicsSolver
     * @param simulation 
     */
    public Space(double G, double timeSpeed, PhysicsSolver physicsSolver, Simulation simulation) {
        super(simulation);
        this.G = G;
        this.timeSpeed = timeSpeed;
        this.physicsSolver = physicsSolver;
    }

    @Override
    public Iterable<Vector> getAcceleration(double t, Iterable<Vector> pos, Iterable<Vector> speed) {
        LinkedList<Vector> accelList = new LinkedList<>();
        Iterator<Vector> posIter = pos.iterator();
        for (GameObject gameObject : getObjects()) {
            accelList.add(gravityCollector.getGravity(G, posIter.next(), gameObject, getObjects(), pos));
        }

        return accelList;
    }

    @Override
    public void update(double dt) {
        double internalDt = dt * timeSpeed;
        for (GameObject object : getObjects()) {
            object.update(internalDt);
        }
        physicsSolver.update(internalDt, getObjects(), this);

        //реализация равномерного вращения
        for (GameObject gameObject : getObjects()) {
            gameObject.setAngle(gameObject.getAngle() + gameObject.getAngleSpeed() * internalDt);
        }
        centerization();

        for (GameObject object : getObjects()) {
            getContainer().registerUpdate(object);
        }
    }

    public void centerization() {
        if (center != null) {
            Vector centerPos = center.getPos();
            Vector centerSpeed = center.getSpeed();
            for (GameObject gameObject : getObjects()) {

                //gameObject.pos.x -= centerPos.x;
                //gameObject.pos.y -= centerPos.y;
                gameObject.setPos(Vector.sum(gameObject.getPos(), Vector.mult(-1, centerPos)));

                //gameObject.speed.x -= centerSpeed.x;
                //gameObject.speed.y -= centerSpeed.y;
                gameObject.setSpeed(Vector.sum(gameObject.getSpeed(), Vector.mult(-1, centerSpeed)));

            }
        }
    }

    public static final String TYPE_NAME = "Space";

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    private class JSON_KEYS extends Level.JSON_KEYS {

        static final String G = "G",
                centerId = "centerId",
                physicsSolver = "physicsSolver",
                timeSpeed = "timeSpeed";

    }

    @Override
    public JSONObject toJson() {
        JSONObject result = super.toJson();

        result.put(JSON_KEYS.G, G);
        result.put(JSON_KEYS.centerId, center.getId());
        result.put(JSON_KEYS.physicsSolver, physicsSolver.getName());
        result.put(JSON_KEYS.timeSpeed, timeSpeed);
        return result;
    }

    private class UnknownPhysicsException extends RuntimeException {
    };

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instanciator) {
        super.updateByJson(json,instanciator);
        G = (double) json.get(JSON_KEYS.G);
        center = (GameObject) instanciator.getUpdated((long) json.get(JSON_KEYS.centerId));
        timeSpeed = (double) json.get(JSON_KEYS.timeSpeed);

        //возможно временно
        String newMethodName = (String) json.get(JSON_KEYS.physicsSolver);
        if (ODEWrapSolver.getName(RungeKutta4.name).equals(newMethodName)) {
            physicsSolver = new ODEWrapSolver(new RungeKutta4());
        } else if (ODEWrapSolver.getName(Euler.name).equals(newMethodName)) {
            physicsSolver = new ODEWrapSolver(new Euler());
        } else {
            throw new UnknownPhysicsException();
        }

    }

    @Override
    public boolean isOutside(Vector v) {
        return false;
    }
    
    @Override
    public void acceptVisitor(ILevelVisitor visitor) {
        visitor.visitSpace(this);
    }
}
