package ru.bel_riose.the_new_gaea_server;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

/**
 * Hello world!
 *
 */
public class App {

    class ServerFrame extends JFrame {

        JTextArea txtAreaLog;
        DefaultListModel connectionsListModel;

        public ServerFrame() throws HeadlessException {
            super("TheNewGaeaServer");
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

            JPanel parametersPanel = new JPanel(new GridLayout(3, 2));

            parametersPanel.add(new JLabel("update dt: "));
            final JTextField txtDt = new JTextField(String.valueOf(server.getDt()));
            parametersPanel.add(txtDt);
            txtDt.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                }

                @Override
                public void focusLost(FocusEvent e) {
                    try {
                        double dt = Double.valueOf(txtDt.getText());
                        if (server.getDt() != dt) {
                            server.setDt(dt);
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Bad value", "Error", JOptionPane.WARNING_MESSAGE);
                        txtDt.setText(String.valueOf(server.getDt()));
                    }
                }
            });

            parametersPanel.add(new JLabel("updates per frame: "));
            final JTextField txtUpdatesPerFrame = new JTextField(String.valueOf(server.getUpdatesPerFrame()));
            parametersPanel.add(txtUpdatesPerFrame);
            txtUpdatesPerFrame.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                }

                @Override
                public void focusLost(FocusEvent e) {
                    try {
                        int updatesPerFrame = Integer.valueOf(txtUpdatesPerFrame.getText());
                        if (server.getUpdatesPerFrame() != updatesPerFrame) {
                            server.setUpdatesPerFrame(updatesPerFrame);
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Bad value", "Error", JOptionPane.WARNING_MESSAGE);
                        txtUpdatesPerFrame.setText(String.valueOf(server.getUpdatesPerFrame()));
                    }
                }
            });

            parametersPanel.add(new JLabel("min frame time (nanosec): "));
            final JTextField txtMinFrameTime = new JTextField(String.valueOf(server.getMinFrameTimeNanosec()));
            parametersPanel.add(txtMinFrameTime);
            txtMinFrameTime.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                }

                @Override
                @SuppressWarnings("empty-statement")
                public void focusLost(FocusEvent e) {
                    try {
                        int minFrameTime = Integer.valueOf(txtMinFrameTime.getText());
                        if (server.getMinFrameTimeNanosec() != minFrameTime) {
                            server.setMinFrameTimeNanosec(minFrameTime);
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Bad value", "Error", JOptionPane.WARNING_MESSAGE);
                        txtMinFrameTime.setText(String.valueOf(server.getMinFrameTimeNanosec()));
                    }
                }
            });

            add(parametersPanel);

            add(new JLabel(" "));
            JLabel lblClients = new JLabel("Connected clients:");
            lblClients.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(lblClients);

            connectionsListModel = new DefaultListModel();
            final JList lstConnections = new JList(connectionsListModel);
            lstConnections.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            add(new JScrollPane(lstConnections));

            //Кнопка "Дисконнект"
            JButton btnDisconnect = new JButton("Disconnect");
            btnDisconnect.setAlignmentX(Component.CENTER_ALIGNMENT);
            btnDisconnect.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedConnection = lstConnections.getSelectedIndex();
                    if (selectedConnection != -1) {
                        server.disconnectPlayer(lstConnections.getMinSelectionIndex());
                    }
                }
            });
            add(btnDisconnect);

            add(new JLabel(" "));
            JLabel lblLog = new JLabel("Server events log:");
            lblLog.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(lblLog);

            txtAreaLog = new JTextArea();
            txtAreaLog.setRows(10);
            txtAreaLog.setEditable(false);

            add(new JScrollPane(txtAreaLog));
            pack();
        }

    }

    private static void setUIFont(javax.swing.plaf.FontUIResource f) {
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof javax.swing.plaf.FontUIResource) {
                UIManager.put(key, f);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        App myApp = new App();
        myApp.run();
    }

    static Server server;
    static ServerFrame serverFrame;

    public void run() throws Exception {
        Simulation sim1 = Simulation.LoadFromFile("../system1.ser");

        ProfilesMap profiles= ProfilesMap.LoadFromFile("../profiles1.json", sim1);

        server = new Server(sim1,profiles, 0.002, 1, 0);

        setUIFont(new javax.swing.plaf.FontUIResource(new Font(Font.DIALOG, Font.PLAIN, 20)));

        serverFrame = new ServerFrame();

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                serverFrame.requestFocus();
            }
        });

        serverFrame.setVisible(true);

        server.start();
    }

    static class WrongThreadException extends RuntimeException {
    };

    /**
     * runs only in GUI thread
     *
     * @param s
     */
    public static void log(String s) {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new WrongThreadException();
        }
        serverFrame.txtAreaLog.append(new SimpleDateFormat("dd.MM.yyy HH:mm:ss").format(Calendar.getInstance().getTime()) + " - " + s + "\r\n");
    }

    /**
     * runs only in GUI thread
     *
     */
    public static void connectionsUpdated() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new WrongThreadException();
        }

        serverFrame.connectionsListModel.clear();
        for (ClientConnection connection : server.getConnections()) {
            String s =connection.getAddress();
            if (connection.getProfile()!=null){
                s=s+" as "+connection.getProfile().getName();               
            }
            serverFrame.connectionsListModel.addElement(s);
        }
    }
}
