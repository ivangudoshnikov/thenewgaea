package ru.bel_riose.thenewgaeatestdesk.html;

import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class TheNewGaeaTestDeskHtml extends GwtApplication {
	@Override
	public ApplicationListener getApplicationListener () {
		return new TheNewGaeaTestDesk();
	}
	
	@Override
	public GwtApplicationConfiguration getConfig () {
		return new GwtApplicationConfiguration(480, 320);
	}
}
