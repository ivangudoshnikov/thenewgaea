/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.gui;

import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenGUI;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreen;

/**
 *
 * @author Bel_Riose
 */
public class AddMenuWindow extends Window {
    final private Vector coords;
    
    public AddMenuWindow(final MapScreen mapScreen,final MapScreenGUI gui, int screenX, int screenY) {
        super("Add..", gui.getSkin());
        float screenYuiScene=-screenY + Gdx.graphics.getHeight();
        Vector mouse= mapScreen.getCamera2d().mouseToScr(screenX, screenY);
        
        coords= mapScreen.getCamera2d().scrToMath(mouse);
        Button btnStar = new TextButton("star", gui.getSkin());
        btnStar.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {                
                Star newStar=new Star("New star", new Vector(coords),0, new Vector(0, 0),0, 0, 0, 0, mapScreen.getApplication().sim.getRoot());
                Gdx.input.setInputProcessor(new InputMultiplexer(gui.getStage(), new AddObjectInputHandler(mapScreen, newStar)));
                gui.setSelectedObject(newStar);
                gui.removeAddMenuWindow();
                gui.addSimState("star added");
            }
        });
        add(btnStar);
        row();
        Button btnPlanet = new TextButton("planet", gui.getSkin());
        btnPlanet.addListener(new ClickListener(Input.Buttons.LEFT) {
            @Override
            public void clicked(InputEvent event, float x, float y) {                
                Planet newPlanet=new Planet("New planet", new Vector(coords),0, new Vector(0, 0),0, 0, 0, mapScreen.getApplication().sim.getRoot(),null);
                Gdx.input.setInputProcessor(new InputMultiplexer(gui.getStage(), new AddObjectInputHandler(mapScreen, newPlanet)));
                gui.setSelectedObject(newPlanet);
                gui.removeAddMenuWindow();
                gui.addSimState("planet added");
            }
        });
        add(btnPlanet);
        pack();
        gui.getStage().addActor(this);
        setPosition(screenX, screenYuiScene);
        setMovable(false);        
    }
}
