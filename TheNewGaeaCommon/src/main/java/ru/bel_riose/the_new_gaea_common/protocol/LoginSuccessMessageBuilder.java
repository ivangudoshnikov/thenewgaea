package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;

/**CurrentCharacterMessage+FullSimUpdateMessage
 *
 * @author Ivan
 */
public class LoginSuccessMessageBuilder extends AbstractMessageBuilder{
    
    public final static String TYPE_NAME = "LoginSuccess";
    
    public class JSON_KEYS extends AbstractMessageBuilder.JSON_KEYS{
        public static final String currentCharacterId = "characterId", sim="sim";        
    }

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    public JSONObject toJson(Profile profile,Simulation sim) {
        JSONObject result = super.toJson();
        JSONObject simJson= sim.writeJsonAll();
        result.put(JSON_KEYS.currentCharacterId, profile.getCurrentCharacter().getId());
        result.put(SimUpdateMessageBuilder.JSON_KEYS.sim, simJson);
        return result;
    }
}
