package ru.bel_riose.thenewgaeatestdesk.core;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.thenewgaeatestdesk.core.TrajectoriesMap.Trajectory;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreen;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.macro_screen.MacroScreen;

public class TheNewGaeaTestDesk extends Game {
    
    private static final Logger log = Logger.getLogger("TestDesk");
    public static final String version = "v. 0.2.2";
    public Simulation sim;
    public TrajectoriesMap trajectories = new TrajectoriesMap();
    private MapScreen mapScreen;
    private MacroScreen macroScreen;
    /**
     * ���������� ������� �������
     */
    private boolean simRunning;
    
    public void CreateDefaultWorld() {
        sim = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new RungeKutta4()), sim);
        sim.setRoot(space);
        trajectories.clear();
    }
    
    @Override
    public void create() {
        simRunning = false;
        try {
            sim = Simulation.LoadFromFile("../../system1.ser");
        } catch (Exception ex) {
            log.log(Level.SEVERE, ex.toString());
            System.exit(1);
        }
        //For external builds
//        CreateDefaultWorld();

        mapScreen = new MapScreen(this);
        macroScreen = new MacroScreen(this);
        setScreen(mapScreen);
    }
    
    public boolean isSimRunning() {
        return simRunning;
    }
    
    public void setSimRunning(boolean simRunning) {
        this.simRunning = simRunning;
    }
    
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
    
    @Override
    public void render() {
        
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        for (GameObject gameObject : sim.getRoot().getObjects()) {
            //���� �������� ������, ��� �������� ��� ������ � ������ ���������� - ������� �����
            if (!trajectories.containsKey(gameObject.getId())) {
                trajectories.put(gameObject.getId(), new Trajectory());
            }
        }

        //���� �� �� �����, �� ��������� ���������� � ����������� ������.
        if (isSimRunning()) {
            for (GameObject gameObject : sim.getRoot().getObjects()) {
                //���� �������� ������, ��� �������� ��� ������ � ������ ���������� - ������� �����
                if (!trajectories.containsKey(gameObject.getId())) {
                    trajectories.put(gameObject.getId(), new Trajectory());
                }
                //��������� ������� ����� � ������ ����������
                trajectories.get(gameObject.getId()).add(new Vector(gameObject.getPos().x, gameObject.getPos().y));
            }
            
            sim.update(1);
        }
        super.render();
    }
    
    public void switchScreens() {
        if (getScreen() == mapScreen) {
            setScreen(macroScreen);
        } else {
            setScreen(mapScreen);
        }
    }
    
    @Override
    public void pause() {
        super.pause();
    }
    
    @Override
    public void resume() {
        super.resume();
    }
    
    @Override
    public void dispose() {
        super.dispose();
    }
    
}
