/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model;

import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.DiskCelestialObject;
import com.cedarsoftware.util.io.JsonReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.numericalmethods.ode.Euler;

/**
 *
 * @author Bel_Riose
 */
public class CloneTest extends TestCase {

    public CloneTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testClone1() {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        GameObject zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        GameObject gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);
        space.setCenter(zun);

        Simulation sim2 = (Simulation) sim1.clone();

        Vector w1Gaea0 = sim1.getRoot().getObjects().getLast().getPos();
        Vector w2Gaea0 = sim2.getRoot().getObjects().getLast().getPos();

        if (!"Gaea".equals(((DiskCelestialObject) sim1.getRoot().getObjects().getLast()).getName())) {
            fail("Order broken");
        }
        if (!"Gaea".equals(((DiskCelestialObject) sim2.getRoot().getObjects().getLast()).getName())) {
            fail("Order broken");
        }

        sim1.update(1);

        Vector w1Gaea1 = sim1.getRoot().getObjects().getLast().getPos();
        Vector w2Gaea1 = sim2.getRoot().getObjects().getLast().getPos();

        assertTrue(!((w1Gaea0.x == w1Gaea1.x) && (w1Gaea0.y == w1Gaea1.y)));
        assertTrue((w2Gaea0.x == w2Gaea1.x) && (w2Gaea0.y == w2Gaea1.y));

        sim2.update(1);
        sim2.update(1);

        Vector w1Gaea2 = sim1.getRoot().getObjects().getLast().getPos();
        Vector w2Gaea2 = sim2.getRoot().getObjects().getLast().getPos();

        assertTrue((w1Gaea1.x == w1Gaea2.x) && (w1Gaea1.y == w1Gaea2.y));
        assertTrue(!((w2Gaea1.x == w2Gaea2.x) && (w2Gaea1.y == w2Gaea2.y)));

    }

    /**
     * проверка на правильность копирования центра
     */
    public void testClone2() {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        GameObject zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        GameObject gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);

        //Устанавливаю центр планетой
        space.setCenter(gaea);

        //Клонирую
        Simulation sim2 = (Simulation) sim1.clone();

        space.setCenter(zun);
        //Гея должна центризоваться
        sim2.update(1);

        DiskCelestialObject sim2Gaea = (DiskCelestialObject) sim2.getRoot().getObjects().getLast();
        if (!"Gaea".equals(sim2Gaea.getName())) {
            fail("order broken!");
        }

        DiskCelestialObject sim2center = (DiskCelestialObject) ((Space) sim2.getRoot()).getCenter();
        if (!"Gaea".equals(sim2center.getName())) {
            fail("center broken");
        }

        assertTrue((sim2center.getPos().x == 0.0) && (sim2center.getPos().y == 0.0));
        assertTrue((sim2center.getPos().x == sim2Gaea.getPos().x) && (sim2center.getPos().y == sim2Gaea.getPos().y));
    }

    /**
     * проверка на правильность копирования центра
     */
    public void testClone3() {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        GameObject zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        GameObject gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);

        //Устанавливаю центр планетой
        space.setCenter(gaea);

        //Клонирую
        Simulation sim2 = (Simulation) sim1.clone();
        assertTrue(sim2.getRoot().getObjects().getLast().getLevel() != sim1.getRoot().getObjects().getLast().getLevel());
    }
}
