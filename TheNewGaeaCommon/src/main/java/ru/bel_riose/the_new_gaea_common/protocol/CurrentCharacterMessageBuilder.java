/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.protocol;

import org.json.simple.JSONObject;

/**
 *
 * @author Ivan
 */
public class CurrentCharacterMessageBuilder extends AbstractMessageBuilder {

    public final static String TYPE_NAME = "CurrentCharacter";

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    public class JSON_KEYS extends AbstractMessageBuilder.JSON_KEYS {
        public static final String currentCharacterId = "characterId";
    }

    public JSONObject toJson(Profile profile) {
        JSONObject result= super.toJson();
        result.put(JSON_KEYS.currentCharacterId, profile.getCurrentCharacter().getId());
        return result;
    }
    
    
    
}
