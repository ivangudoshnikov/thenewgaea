/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.gui;

import com.badlogic.gdx.Input;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.space.DiskCelestialObject;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreen;
import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenInputHandler;

/**
 *
 * @author Bel_Riose
 */
public class AddObjectInputHandler extends MapScreenInputHandler {

    private DiskCelestialObject object;

    public AddObjectInputHandler(MapScreen mapScreen, DiskCelestialObject object) {
        super(mapScreen);
        this.object = object;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        dragHandler.down(screenX, screenY, button);
        screen.getGui().getStage().setKeyboardFocus(null);
        screen.getGui().getStage().setScrollFocus(null);
        Vector click = getCamera().mouseToScr(screenX, screenY);
        if (Input.Buttons.LEFT == button) {
            boolean empty = true;
            for (GameObject gameObject : screen.getApplication().sim.getRoot().getObjects()) {
                Vector p = getCamera().scrToMath(click);
                if (gameObject.isInside(p)) {
                    empty = false;
                    break;
                }
            }
            if (empty) {
                screen.setDeafultInputProcessor();
            }
        } else if (Input.Buttons.RIGHT == button) {

            object.delete();

            screen.getApplication().trajectories.remove(screen.getApplication().trajectories.get(object));
            screen.getGui().setSelectedObject(null);
            screen.setDeafultInputProcessor();
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Vector mouse = getCamera().mouseToScr(screenX, screenY);
        Vector pos = object.getPos();
        Vector click = getCamera().scrToMath(mouse);
        object.setR(Math.hypot(pos.x - click.x, pos.y - click.y));
        screen.getGui().update();
        return true;
    }
}
