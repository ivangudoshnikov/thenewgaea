/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.map_screen;

import com.badlogic.gdx.Input;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.MapInputHandler;

/**
 *
 * @author Bel_Riose
 */
public class MapScreenInputHandler extends MapInputHandler<MapScreen> {

    public MapScreenInputHandler(MapScreen screen) {
        super(screen);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.BACKSPACE:
                getCamera().restoreDefaults();
                break;
            case Input.Keys.ESCAPE:
                System.exit(0);
                break;
            case Input.Keys.SPACE:
                screen.getApplication().setSimRunning(!screen.getApplication().isSimRunning());
                break;
            case Input.Keys.TAB:
                screen.getApplication().switchScreens();
                break;
        }
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        super.touchDown(screenX, screenY, pointer, button);
        screen.getGui().getStage().setKeyboardFocus(null);
        screen.getGui().getStage().setScrollFocus(null);
        Vector click = getCamera().mouseToScr(screenX,screenY);
        //����� ����
        boolean selected = false;
        if (Input.Buttons.LEFT == button) {
            screen.getGui().removeAddMenuWindow();
            screen.getGui().removeAddMenuWindow();
            for (GameObject gameObject : screen.getApplication().sim.getRoot().getObjects()) {
                Vector p = getCamera().scrToMath(click);
                if (gameObject.isInside(p)) {
                    screen.getGui().setSelectedObject(gameObject);
                    selected = true;
                    break;
                }
            }
            if (!selected) {
                screen.getGui().setSelectedObject(null);
            }            
        } else if (Input.Buttons.RIGHT == button) {
            screen.getGui().removeAddMenuWindow();
            boolean empty = true;
            for (GameObject gameObject : screen.getApplication().sim.getRoot().getObjects()) {
                Vector p = getCamera().scrToMath(click);
                if (gameObject.isInside(p)) {
                    empty = false;
                    break;
                }
            }
            if (empty) {
                screen.getGui().showAddMenuWindow(screenX, screenY);
            }
        }
        return true;
    }

    @Override
    protected MapCamera2D getCamera() {
        return screen.getCamera2d();
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector mouse = getCamera().mouseToScr(screenX, screenY);
        if (dragHandler.button == Input.Buttons.MIDDLE) {
            getCamera().drag(screenX, screenY, dragHandler.lastX, dragHandler.lastY);
        } else if (dragHandler.button == Input.Buttons.LEFT) {
            GameObject selectedObject = screen.getGui().getSelectedObject();
            if (selectedObject != null) {
                boolean empty = true;
                for (GameObject gameObject : screen.getApplication().sim.getRoot().getObjects()) {
                    Vector p = getCamera().scrToMath(mouse);
                    if (gameObject.isInside(p)) {
                        empty = false;
                        break;
                    }
                }
                if (empty) {
                    selectedObject.setPos(getCamera().scrToMath(mouse));
                    screen.getApplication().trajectories.get(selectedObject.getId()).clear();
                    screen.getGui().update();
                }
            }
        }

        dragHandler.drag(screenX, screenY);
        return true;
    }
}
