package ru.bel_riose.thenewgaeanumericaltest;

import ru.bel_riose.geometry.Vector;
import ru.bel_riose.numericalmethods.ode.Euler;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;


class Checker{
    private double v0,r0,G,M;

        public Checker(double v0, double r0, double G, double M) {
            this.v0 = v0;
            this.r0 = r0;
            this.G = G;
            this.M = M;
        }
    
        public double r(double phi){
            double q=r0*v0*v0/(G*M);
            return q*r0/(1+(q-1)*Math.cos(phi));            
        }
    }

public class App 
{
    
    
    public static void main( String[] args )
    {
        Simulation systemRK =new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new RungeKutta4()) ,systemRK);
        systemRK.setRoot(space);
        GameObject zun=new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0,10, space);        
        GameObject gaea=new Planet("Gaea", 1, 0, 0, 0.13, 0, 400, 0, 0, space,null);        
        space.setCenter(zun);
              
        Simulation systemEULER= (Simulation) systemRK.clone();
        ((Space)systemEULER.getRoot()).setPhysics(new ODEWrapSolver(new Euler()));
                
        Checker check= new Checker(0.13, 400, 1, 10);
        long count=0;
        while (true){
            Vector pEU=systemEULER.getRoot().getObjects().getLast().getPos();
            double phiEU=Math.atan2(pEU.y, pEU.x);
            double rEU=Math.hypot(pEU.x, pEU.y);
            double elliptic_r_EU=check.r(phiEU);
            
            Vector pRK=systemRK.getRoot().getObjects().getLast().getPos();
            double phiRK=Math.atan2(pRK.y, pRK.x);
            double rRK=Math.hypot(pRK.x, pRK.y);
            double elliptic_r_RK=check.r(phiRK);
                        
            System.out.println(Long.toString(count)+" Euler diff: "+Double.toString(rEU-elliptic_r_EU)+"  RK diff: "+Double.toString(rRK-elliptic_r_RK));
            systemEULER.update(1);
            systemRK.update(1);        
            count++;
        }
    }
}
