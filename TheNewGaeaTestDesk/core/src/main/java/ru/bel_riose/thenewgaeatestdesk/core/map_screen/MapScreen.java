package ru.bel_riose.thenewgaeatestdesk.core.map_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.LinkedList;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.view.MapDrawer;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;
import ru.bel_riose.thenewgaeatestdesk.core.TheNewGaeaTestDesk;
import ru.bel_riose.thenewgaeatestdesk.core.gui.ParamWindowFactory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ivan
 */
public class MapScreen extends Screen<TheNewGaeaTestDesk,MapCamera2D,MapScreenInputHandler,MapScreenGUI> {    
    final private MapDrawer drawer = new MapDrawer();
    final public ParamWindowFactory paramWindowFacrory= new ParamWindowFactory();
   
    public MapScreen(TheNewGaeaTestDesk application) {
        super(application);
        camera2d = new MapCamera2D(0, 0, 1,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        gui = new MapScreenGUI(this);
        defaultInputHandler= new MapScreenInputHandler(this);
    }

    @Override
    public void render(float f) {        
        actualRender();
        if(application.isSimRunning())gui.update();
        gui.actNdraw();
    }

    void actualRender() {        
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        shapeRenderer.setColor(Color.WHITE);
        //����� ����������
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        for (GameObject gameObject : application.sim.getRoot().getObjects()) {
            LinkedList<Vector> trajectory = application.trajectories.get(gameObject.getId());
            Vector prev = null;
            for (Vector point : trajectory) {
                if (prev == null) {
                    prev = point;
                }                
                shapeRenderer.line((float) prev.x, (float) prev.y, (float) point.x, (float) point.y);
                prev = point;
            }
        }
        shapeRenderer.end();
        //����� ����
        for (GameObject gameObject : application.sim.getRoot().getObjects()) {
            drawer.draw(gameObject, shapeRenderer, camera2d);            
        }
    }    
}
