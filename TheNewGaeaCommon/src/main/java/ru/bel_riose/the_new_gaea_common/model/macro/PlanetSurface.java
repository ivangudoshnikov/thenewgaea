/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.macro;

import org.json.simple.JSONObject;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.ILevelVisitor;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.transferable.EntityUpdater;

/**
 *
 * @author Ivan
 */
public class PlanetSurface extends Level {

    /**
     * terrain
     */
    private TerrainMesh terrainMesh;

    /**
     * Space object
     */
    private Planet planet;
    private double h, r;

    /**
     * For json parsers
     */
    public PlanetSurface() {
    }

    /**
     * Conventional constructor
     *
     * @param simulation
     * @param r average terrain radius
     * @param h atmosphere height
     */
    public PlanetSurface(Simulation simulation, double r, double h) {
        super(simulation);
        terrainMesh = new TerrainMesh(simulation, r);
        this.r = r;
        this.h = h;
    }

    @Override
    public void update(double dt) {

    }

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }

    @Override
    public void acceptVisitor(ILevelVisitor visitor) {
        visitor.visitPlanetSurface(this);
    }

    public class JSON_KEYS extends Level.JSON_KEYS {

        public static final String terrainMeshId = "terrainMeshId", h = "h", r = "r";
    }

    public static final String TYPE_NAME = "PlanetSurface";

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    @Override
    public void updateByJson(JSONObject json, EntityUpdater instanciator) {
        super.updateByJson(json, instanciator);
        r = (Double) json.get(JSON_KEYS.r);
        h = (Double) json.get(JSON_KEYS.h);
        terrainMesh = (TerrainMesh) instanciator.getUpdated((Long) json.get(JSON_KEYS.terrainMeshId));
    }

    @Override
    public JSONObject toJson() {
        JSONObject result = super.toJson();
        result.put(JSON_KEYS.r, r);
        result.put(JSON_KEYS.h, h);
        result.put(JSON_KEYS.terrainMeshId, terrainMesh.getId());
        return result;
    }

    public TerrainMesh getTerrainMesh() {
        return terrainMesh;
    }

    public Planet getPlanet() {
        return planet;
    }

    @Override
    public boolean isOutside(Vector v) {
        return Math.hypot(v.x, v.y) > (h + r);
    }

    public double getH() {
        return h;
    }

    public double getR() {
        return r;
    }
}
