/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core.map_screen;

import com.badlogic.gdx.Gdx;
import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.view.MapDrawer;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;

/**
 *
 * @author Bel_Riose
 */
public class MapScreen extends Screen<TheNewGaeaClient,MapCamera2D,MapScreenInputHandler,MapScreenGUI> {

    long selectedId = -1;
    public static final MapDrawer drawer = new MapDrawer();
    public static final InfoWindowFactory infoWindowMapper = new InfoWindowFactory();
    
    public MapScreen(TheNewGaeaClient application) {
        super(application);
        camera2d = new MapCamera2D(0, 0, 1,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        gui = new MapScreenGUI(this);
        defaultInputHandler= new MapScreenInputHandler(this);
    }

    @Override
    public void render(float delta) {
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        Level root = application.getSim().getRoot();
        if (root != null) {
            for (GameObject gameObject : root.getObjects()) {
                drawer.draw(gameObject, shapeRenderer, camera2d);
            }
        }
        gui.update();
        gui.actNdraw();
    }  
}
