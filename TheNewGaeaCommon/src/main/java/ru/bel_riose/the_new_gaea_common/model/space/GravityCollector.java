/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model.space;

import java.io.Serializable;
import java.util.Iterator;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;


/**
 *
 * @author Ivan
 */
public class GravityCollector implements Serializable{

    private final GravityCollectorImpl impl = new GravityCollectorImpl();

    public Vector getGravity(double G, Vector target, GameObject ignored, Iterable<GameObject> objects, Iterable<Vector> positionList) {
        return impl.getGravity(G, target, ignored, objects, positionList);
    }

    private class GravityCollectorImpl implements IGameObjectVisitor,Serializable{

        private Vector gravity, position, target;
        
        public Vector getGravity(double G, Vector target, GameObject ignored, Iterable<GameObject> objects, Iterable<Vector> positionList) {
            gravity = new Vector(0, 0);
            this.target = target;
            Iterator<Vector> positionIter = positionList.iterator();
            for (GameObject gameObject : objects) {
                position = positionIter.next();
                if (gameObject != ignored) {
                    gameObject.acceptVisitor(this);
                }
            }
            return gravity.multMe(G);
        }

        private void countDiskCelestialObjectGravity(DiskCelestialObject object) {
            double distance = Math.hypot(position.x - target.x, position.y - target.y);
            gravity.x += object.getM() * (position.x - target.x) / (distance * distance * distance);
            gravity.y += object.getM() * (position.y - target.y) / (distance * distance * distance);
        }

        @Override
        public void visitPlanet(Planet planet) {
            countDiskCelestialObjectGravity(planet);
        }

        @Override
        public void visitStar(Star star) {
            countDiskCelestialObjectGravity(star);
        }

        @Override
        public void visitCharacter(Character character) {
        }
    }
}
