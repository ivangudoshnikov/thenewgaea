/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_common.model;

import com.cedarsoftware.util.io.JsonReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.bel_riose.numericalmethods.kinematics2d.ODEWrapSolver;
import ru.bel_riose.numericalmethods.ode.Euler;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;
import ru.bel_riose.the_new_gaea_common.model.space.DiskCelestialObject;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;

/**
 *
 * @author Ivan
 */
public class JsonTest extends TestCase {

    public JsonTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testJsonIo1() throws IOException {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        GameObject zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        GameObject gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);

        //Устанавливаю центр планетой
        space.setCenter(gaea);

        String json = Simulation.writeAllJsonIo(sim1);
        Simulation sim2 = (Simulation) JsonReader.jsonToJava(json);
    }

    public void testJsonIo2() throws IOException {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        Star zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        Planet gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);

        //Устанавливаю центр планетой
        space.setCenter(gaea);

        String json = Simulation.writeAllJsonIo(sim1);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintWriter printWriter = new PrintWriter(byteArrayOutputStream);
        printWriter.print(json);
        printWriter.close();

        JsonReader jsonReader = new JsonReader(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
        Simulation sim2 = (Simulation) jsonReader.readObject();
        System.out.println(((DiskCelestialObject) (sim2.getRoot().getObjects().getLast())).getName());
        assertEquals(gaea.getName(), ((DiskCelestialObject) (sim2.getRoot().getObjects().getLast())).getName());
    }

    public void testMyJson1() throws IOException {
        Simulation sim1 = new Simulation();
        Space space = new Space(1, 4, new ODEWrapSolver(new Euler()), sim1);
        sim1.setRoot(space);
        GameObject zun = new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10, space);
        GameObject gaea = new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, space,null);

        //Устанавливаю центр планетой
        space.setCenter(gaea);

        System.out.println("Custom JSON serialization:");
        System.out.println(sim1.writeJsonAll());
        sim1.update(1);
        System.out.println(sim1.writeJsonOnlyUpdated());
        zun.delete();
        System.out.println(sim1.writeJsonOnlyUpdated());

    }

    public void testMyJson2() throws ParseException {
        Simulation simulation1 = new Simulation();
        Space system1 = new Space(1, 4, new ODEWrapSolver(new RungeKutta4()),simulation1);
        simulation1.setRoot(system1);
        GameObject zun=new Star("Zun", 100, 10, 0, 0, 0, 0, 0, 0, 10,system1);        
        GameObject gaea=new Planet("Gaea", 1, 0.1, 0.13, 0, 0, 0, 400, 0, system1,null);        
        GameObject zeus=new Planet("Zeus", 10, 1, -0.11, 0, 0, 0, -1000, 0, system1,null);
        GameObject gefest=new Planet("Gefest", 0.5, 0.05, 0.1, 0, 0, 0, -1020, 0, system1,null);
        system1.setCenter(zun);
        
        //initial tranfer
        JSONObject json = simulation1.writeJsonAll();
        String str = json.toString();
        System.out.println(str);
        JSONParser jsonParser = new JSONParser();
        System.out.println("initial transfer test");
        JSONObject json2= (JSONObject) jsonParser.parse(str);
        Simulation simulation2 = new Simulation();
        simulation2.updateByJson(json2);
        
        //update transfer
        simulation1.update(1);
        json=simulation1.writeJsonOnlyUpdated();
        str=json.toString();
        System.out.println("update transfer test");
        System.out.println(str);
        json2= (JSONObject) jsonParser.parse(str);
        simulation2.updateByJson(json2);
        
        //independent updates
        simulation1.update(1);
        simulation2.update(1);
        assertTrue(((Planet)simulation1.getEntity(2)).getPos().x==((Planet)simulation2.getEntity(2)).getPos().x);
    
    }

}
