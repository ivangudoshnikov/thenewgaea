/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core.map_screen;

import com.badlogic.gdx.Input;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.MapInputHandler;

/**
 *
 * @author Bel_Riose
 */
class MapScreenInputHandler extends MapInputHandler<MapScreen>{
    public MapScreenInputHandler(MapScreen screen) {
        super(screen);
    }
    
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.BACKSPACE:
                screen.getCamera2d().restoreDefaults();
                break;
            case Input.Keys.ESCAPE:
                System.exit(0);
                break;
            case Input.Keys.TAB:
                screen.getApplication().switchScreen();
        }
        return true;
    }
    
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        super.touchDown(screenX, screenY, pointer, button);
        screen.getGui().getStage().setKeyboardFocus(null);
        Vector click= getCamera().mouseToScr(screenX, screenY);
        //����� ����
        boolean selected = false;
        if (Input.Buttons.LEFT == button) {
            for (GameObject gameObject : screen.getApplication().getSim().getRoot().getObjects()) {
                Vector p = getCamera().scrToMath(click);
                if (gameObject.isInside(p)) {
                    screen.selectedId = gameObject.getId();
                    screen.getGui().showInfoWindow(gameObject);
                    selected = true;
                    break;
                }
            }
            if (!selected) {
                screen.selectedId=-1;
                screen.getGui().removeInfoWindow();
            }            
        }
        return true;
    }

    @Override
    protected MapCamera2D getCamera() {
        return screen.getCamera2d();
    }
    
}
