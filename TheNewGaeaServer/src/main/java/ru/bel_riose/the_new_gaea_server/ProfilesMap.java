/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_server;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

/**
 *
 * @author Ivan
 */
public class ProfilesMap extends HashMap<String, Profile> {

    
    
    public static ProfilesMap LoadFromFile(String filename, Simulation sim) throws IOException {
        ProfilesMap result = new ProfilesMap();
        try {
            JSONParser jsonParser = new JSONParser();
            JSONArray profilesJson = (JSONArray) jsonParser.parse(new FileReader(filename));
            for (Object profileJson : profilesJson) {
                Profile profile = Profile.fromJson((JSONObject) profileJson, sim);
                result.put(profile.getName(), profile);
            }
        } catch (ParseException ex) {
            Logger.getLogger(ProfilesMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

}
