/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core;

import static junit.framework.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.thenewgaeatestdesk.core.TrajectoriesMap.Trajectory;


/**
 *
 * @author Bel_Riose
 */
public class TheNewGaeaTestDeskTest {
    
    public TheNewGaeaTestDeskTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTrajectoriesMapClone() {
        long id=1;
        TrajectoriesMap trajectories = new TrajectoriesMap();
        trajectories.put(id, new Trajectory());
        trajectories.get(id).add(new Vector(1, 1));
        TrajectoriesMap trClone=(TrajectoriesMap) trajectories.clone();
        
        trClone.get(id).add(new Vector(2, 2));
        System.out.println(trajectories.get(id).size());
        System.out.println(trClone.get(id).size());
        
        assertTrue(trajectories.get(id).size()<trClone.get(id).size()); 
    }
    @Test
    public void testTrajectoriesClone() {
        
        Trajectory trajectory = new Trajectory();
        trajectory.add(new Vector(1, 1));
        Trajectory trClone=(Trajectory) trajectory.clone();
        trClone.getFirst().x=2;
        System.out.println(trajectory.getFirst().x);
        System.out.println(trClone.getFirst().x);
        
        assertTrue(trajectory.getFirst().x!=trClone.getFirst().x); 
    }
}