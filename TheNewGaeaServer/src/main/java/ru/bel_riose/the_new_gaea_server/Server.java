package ru.bel_riose.the_new_gaea_server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.bel_riose.the_new_gaea_common.model.Simulation;
import ru.bel_riose.the_new_gaea_common.protocol.LoginMessageBuilder;
import ru.bel_riose.the_new_gaea_common.protocol.Profile;

/**
 *
 * @author Bel_Riose
 */
public class Server extends Thread {

    public Server(Simulation sim, ProfilesMap profiles, double dt, int updatesPerFrame, int minFrameTimeNanosec) {
        this.sim = sim;
        this.dt = dt;
        this.updatesPerFrame = updatesPerFrame;
        this.minFrameTimeNanosec = minFrameTimeNanosec;
        this.profiles = profiles;
    }
    private final ProfilesMap profiles;
    private final Simulation sim;
    private volatile double dt;
    private volatile int updatesPerFrame, minFrameTimeNanosec;
    private final CopyOnWriteArrayList<ClientConnection> connected = new CopyOnWriteArrayList<>();
    private String fullJson, updateJson;

    private final JSONParser jsonParser = new JSONParser();

    /**
     * Принимает новые соединения
     */
    class NewConnectionsWatchThread extends Thread {

        @Override
        public void run() {
            try {
                ServerSocket ss = new ServerSocket(2100);
                while (true) {
                    Socket s = ss.accept();
                    ClientConnection newConnection = new ClientConnection(s);
                    connected.add(newConnection);
                    //gui update
                    log(newConnection.getAddress() + " connected");
                    connectionsUpdated();
                }
            } catch (IOException e) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private String getFullUpdateMessage() {
        if (fullJson == null) {
            fullJson = MessageManager.getSimUpdateMessage(sim, true).toString();
        }
        return fullJson;
    }

    private String getUpdateMessage() {
        if (updateJson == null) {
            updateJson = MessageManager.getSimUpdateMessage(sim, false).toString();
        }
        return updateJson;
    }

    public double getDt() {
        return dt;
    }

    public CopyOnWriteArrayList<ClientConnection> getConnections() {
        return connected;
    }

    public int getUpdatesPerFrame() {
        return updatesPerFrame;
    }

    public int getMinFrameTimeNanosec() {
        return minFrameTimeNanosec;
    }

    public void setDt(double dt) {
        this.dt = dt;
        log("dt set to " + String.valueOf(dt));
    }

    public void setUpdatesPerFrame(int updatesPerFrame) {
        this.updatesPerFrame = updatesPerFrame;
        log("updates per frame set to " + String.valueOf(updatesPerFrame));
    }

    public void setMinFrameTimeNanosec(int minFrameTimeNanosec) {
        this.minFrameTimeNanosec = minFrameTimeNanosec;
        log("min frame time set to " + String.valueOf(minFrameTimeNanosec) + " seconds");
    }

    void log(final String s) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                App.log(s);
            }
        });
    }

    /**
     * show changes at ui
     */
    void connectionsUpdated() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                App.connectionsUpdated();
            }
        });
    }

    public void disconnectPlayer(int i) {
        ClientConnection disconnecting = connected.get(i);
        connected.remove(disconnecting);
        disconnecting.disconnect();
        log(disconnecting.getAddress() + " disconnected by server");
        connectionsUpdated();
    }

    private void workWithConnections() {
        for (ClientConnection connection : connected) {
            try {
                while (connection.available() != 0) {
                    JSONObject message = (JSONObject) jsonParser.parse(connection.read());
                    MessageManager.processMessage(message, connection, this);
                }

                if (connection.loggedIn) {
                    connection.write(getUpdateMessage());
                }
            } catch (IOException ex) {
                connection.disconnect();
                connected.remove(connection);
                log(connection.getAddress() + " disconnected by himself");
                connectionsUpdated();
            } catch (ParseException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void login(JSONObject message, ClientConnection connection) {
        try {
            String name = (String) message.get(LoginMessageBuilder.JSON_KEYS.name);
            String password = (String) message.get(LoginMessageBuilder.JSON_KEYS.password);
            Profile profile = profiles.get(name);
            if (profile == null) {
                connection.write(MessageManager.getLoginRejectedMessage("No such profile found").toString());
                log(connection.getAddress() + " tried to login as a stranger " + name);
            } else if (profile.getConnection() != null) {
                connection.write(MessageManager.getLoginRejectedMessage("Profile ocuuped").toString());
                log(connection.getAddress() + " tried to login as occuped " + name);
            } else if (!profile.getPassword().equals(password)) {
                connection.write(MessageManager.getLoginRejectedMessage("Wrong password").toString());
                log(connection.getAddress() + " tried to login as " + name + " with wrong password");
            } else {
                connection.write(MessageManager.getLoginSuccessMessage(profile,sim).toString());
                connection.setProfile(profile);
                profile.setConnection(connection);
                log(connection.getAddress() + " logged in as " + name);
                connection.loggedIn = true;
                connectionsUpdated();
            }
        } catch (IOException e) {
        }
    }

    @Override
    public void run() {
        Thread networkInputThread = new NewConnectionsWatchThread();
        networkInputThread.start();
        log("Server started");

        long previousCycleTime, currentCycleStartTime = System.nanoTime();
        while (true) {
            previousCycleTime = currentCycleStartTime;
            currentCycleStartTime = System.nanoTime();
            long realDtNano = currentCycleStartTime - previousCycleTime; //not used

            for (int i = 0; i < updatesPerFrame; i++) {
                sim.update(dt);
            }

            fullJson = null;
            updateJson = null;

            workWithConnections();

            //uniform frame delay
            while (currentCycleStartTime + minFrameTimeNanosec > System.nanoTime()) {
            }
        }
    }
}
