package ru.bel_riose.the_new_gaea_client.core.macro_screen;

import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Ivan
 */
class MacroScreenGUI extends GraphicalUserInterface<MacroScreen>{

    public MacroScreenGUI(MacroScreen screen) {
        super(screen);
    }
    
    @Override
    public void update() {
        
    }
}
