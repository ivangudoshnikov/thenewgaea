package ru.bel_riose.the_new_gaea_client.core.map_screen;

import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Ivan
 */
class MapScreenGUI extends GraphicalUserInterface<MapScreen> {

    private InfoWindowFactory.InfoWindow winObjectInfo;

    public MapScreenGUI(MapScreen mapScreen) {
        super(mapScreen);
    }

    public void showInfoWindow(GameObject object) {
        removeInfoWindow();
        winObjectInfo = MapScreen.infoWindowMapper.getInfoWindow(object, this);
        winObjectInfo.pack();
        winObjectInfo.setPosition(0, 0);
        stage.addActor(winObjectInfo);

    }

    public void removeInfoWindow() {
        if (winObjectInfo != null) {
            winObjectInfo.remove();
            winObjectInfo = null;
        }
    }

    @Override
    public void update() {
        if (winObjectInfo != null) {
            winObjectInfo.update(screen.getApplication().getSim());
        }
    }
}
