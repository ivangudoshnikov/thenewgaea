/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.map_screen.macro_screen;

import com.badlogic.gdx.Input;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.IGameObjectVisitor;
import ru.bel_riose.the_new_gaea_common.model.ILevelVisitor;
import ru.bel_riose.the_new_gaea_common.model.macro.Character;
import ru.bel_riose.the_new_gaea_common.model.macro.PlanetSurface;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.model.space.Star;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.MapInputHandler;

/**
 *
 * @author Bel_Riose
 */
public class MacroScreenInputHandler extends MapInputHandler<MacroScreen> {
    
    private final GoInsideLevelVisitor goInsideLevelVisitor;
    private final GoOutsideLevelVisitor goOutsideLevelVisitor;
    
    public MacroScreenInputHandler(MacroScreen screen) {
        super(screen);
        goInsideLevelVisitor = new GoInsideLevelVisitor();
        goOutsideLevelVisitor=new GoOutsideLevelVisitor();
    }
    
    @Override
    protected MapCamera2D getCamera() {
        return screen.getCamera2d();
    }
    
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.BACKSPACE:
                getCamera().restoreDefaults();
                break;
            case Input.Keys.ESCAPE:
                System.exit(0);
                break;
            case Input.Keys.SPACE:
                screen.getApplication().setSimRunning(!screen.getApplication().isSimRunning());
                break;
            case Input.Keys.TAB:
                screen.getApplication().switchScreens();
                break;
        }
        return true;
    }
    
    @Override
    public boolean scrolled(int amount) {
        super.scrolled(amount);
        screen.getCurrentLevel().acceptVisitor(goOutsideLevelVisitor);
        for (GameObject gameObject : screen.getCurrentLevel().getObjects()) {
            gameObject.acceptVisitor(goInsideLevelVisitor);
        }
        return true;
    }

    /**
     * Check for zoom inside level
     *
     */
    private class GoInsideLevelVisitor implements IGameObjectVisitor {
        
        @Override
        public void visitPlanet(Planet planet) {
            MapCamera2D camera = getCamera();
            if (planet.getSurface() != null
                    && planet.isInside(camera.scrToMath(-camera.getWidth() / 2, -camera.getHeight() / 2))
                    && planet.isInside(camera.scrToMath(camera.getWidth() / 2, camera.getHeight() / 2))) {
                screen.setCurrentLevel(planet.getSurface());
                camera.setD(new Vector(0, 0));
                camera.setK(camera.getK() * 4);
            }
        }
        
        @Override
        public void visitStar(Star star) {
        }
        
        @Override
        public void visitCharacter(Character character) {
        }
    }
    
    private class GoOutsideLevelVisitor implements ILevelVisitor {
        
        @Override
        public void visitSpace(Space space) {
        }
        
        @Override
        public void visitPlanetSurface(PlanetSurface surface) {
            if (surface.getPlanet() != null) {
                Vector a = getCamera().mathToScr(new Vector(-surface.getH() - surface.getR(), 0)),
                        b = getCamera().mathToScr(new Vector(surface.getH() + surface.getR(), 0));
                if (Math.hypot(a.x - b.x, a.y - b.y) < Math.min(getCamera().getWidth(), getCamera().getHeight()) / 20) {
                    screen.setCurrentLevel(surface.getPlanet().getLevel());
                    getCamera().setD(Vector.sum(getCamera().getD(),surface.getPlanet().getPos()));
                }
            }
        }
        
    }
}
