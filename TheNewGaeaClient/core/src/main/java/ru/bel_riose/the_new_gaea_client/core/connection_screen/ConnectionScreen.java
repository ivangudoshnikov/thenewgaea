/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.the_new_gaea_client.core.connection_screen;

import com.badlogic.gdx.Gdx;
import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;
import ru.bel_riose.libgdx_2d_skeleton.Camera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;

/**
 *
 * @author Bel_Riose
 */
public class ConnectionScreen extends Screen<TheNewGaeaClient,Camera2D,ConnectionScreenInputHandler,ConnectionScreenGUI> {

    public ConnectionScreen(TheNewGaeaClient application) {
        super(application);        
        camera2d = new Camera2D(0, 0, 1,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());  
        gui=new ConnectionScreenGUI(this);
        defaultInputHandler= new ConnectionScreenInputHandler(this);        
    }
    
    public void showLoginRejectedMessage(String reason){
        gui.showLoginRejectedMessage(reason);
    }

    @Override
    public void render(float delta) {
        gui.actNdraw();
    }
}
