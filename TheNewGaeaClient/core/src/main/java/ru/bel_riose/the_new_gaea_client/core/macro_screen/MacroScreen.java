package ru.bel_riose.the_new_gaea_client.core.macro_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import ru.bel_riose.the_new_gaea_client.core.TheNewGaeaClient;
import ru.bel_riose.the_new_gaea_common.model.GameObject;
import ru.bel_riose.the_new_gaea_common.model.Level;
import ru.bel_riose.the_new_gaea_common.view.MacroDrawer;
import ru.bel_riose.libgdx_2d_skeleton.Camera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;

/**
 *
 * @author Ivan
 */
public class MacroScreen extends Screen<TheNewGaeaClient,Camera2D,MacroScreenInputHandler,MacroScreenGUI> {    
    private final MacroDrawer drawer = new MacroDrawer();

    public MacroScreen(TheNewGaeaClient application) {
        super(application);
        camera2d = new Camera2D(0, 0, 1,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());        
        gui = new MacroScreenGUI(this);
        defaultInputHandler= new MacroScreenInputHandler(this);
    }

    @Override
    public void render(float f) {
        Gdx.gl.glClearColor(0.01f, 0.7f, 0.9f, 1); //cyan background
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        camera2d.setD(application.getProfile().getCurrentCharacter().getPos());
        camera2d.setAngle(application.getProfile().getCurrentCharacter().getAngle());
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        spriteBatch.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        Level level = application.getProfile().getCurrentCharacter().getLevel();
        drawer.draw(level, shapeRenderer,spriteBatch, camera2d);
        for (GameObject gameObject : level.getObjects()) {
            drawer.draw(gameObject, shapeRenderer,spriteBatch, camera2d);
        }

        gui.update();
        gui.actNdraw();
    }
}
