/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.thenewgaeatestdesk.core.gui;

import ru.bel_riose.thenewgaeatestdesk.core.map_screen.MapScreenGUI;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.the_new_gaea_common.model.space.Planet;
import ru.bel_riose.the_new_gaea_common.model.space.Space;
import ru.bel_riose.the_new_gaea_common.view.gui.Message;
import ru.bel_riose.thenewgaeatestdesk.core.gui.ParamWindowFactory.ParamWindow;
   /**
     *
     * @author Bel_Riose
     */
    public class PlanetParamWindow extends ParamWindow<Planet> {
        
        private TextField txtName, txtX, txtY, txtVX, txtVY, txtM, txtR;
        private TextButton btnUpdate, btnSetCenter, btnDelete;
        
        public PlanetParamWindow(Planet planet, final MapScreenGUI gui) {
            super(planet, "Planet parameters", gui);
            
            add(new Label("Name: ", gui.getSkin()));
            txtName = new TextField(planet.getName(), gui.getSkin());
            add(txtName);
            row();
            
            Vector p = planet.getPos();
            add(new Label("Coords: ", gui.getSkin()));
            txtX = new TextField(String.valueOf(p.x), gui.getSkin());
            add(txtX);
            txtY = new TextField(String.valueOf(p.y), gui.getSkin());
            add(txtY);
            row();
            
            Vector v = planet.getSpeed();
            add(new Label("Speed: ", gui.getSkin()));
            txtVX = new TextField(String.valueOf(v.x), gui.getSkin());
            add(txtVX);
            txtVY = new TextField(String.valueOf(v.y), gui.getSkin());
            add(txtVY);
            row();
            
            add(new Label("M: ", gui.getSkin()));
            txtM = new TextField(String.valueOf(planet.getM()), gui.getSkin());
            add(txtM);
            row();
            
            add(new Label("R: ", gui.getSkin()));
            txtR = new TextField(String.valueOf(planet.getR()), gui.getSkin());
            add(txtR);
            row();
            
            btnUpdate = new TextButton("UPDATE", gui.getSkin());
            btnUpdate.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    try {
                        //��������� ���������� ���������
                        //TODO: ���� �����!
                        Vector newPos = new Vector(Double.valueOf(txtX.getText()), Double.valueOf(txtY.getText()));
                        Vector newSpeed = new Vector(Double.valueOf(txtVX.getText()), Double.valueOf(txtVY.getText()));
                        double newM = Double.valueOf(txtM.getText());
                        double newR = Double.valueOf(txtR.getText());
                        object.setName(txtName.getText());
                        object.setPos(newPos);
                        object.setSpeed(newSpeed);
                        object.setM(newM);
                        object.setR(newR);
                        gui.addSimState(object.getName() + " updated");
                    } catch (Exception ex) {
                        new Message("Error", "Can't update parameters", gui.getStage(), gui.getSkin());
                    }
                }
            });
            add(btnUpdate);
            btnSetCenter = new TextButton("Make center", gui.getSkin());
            btnSetCenter.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    ((Space) object.getLevel()).setCenter(object);
                    gui.getScreen().getApplication().trajectories.clear();
                    gui.update();
                    
                }
            });
            add(btnSetCenter);
            btnDelete = new TextButton("Delete", gui.getSkin());
            btnDelete.addListener(new ClickListener(Input.Buttons.LEFT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    //TODO: delete method
                    object.delete();
                    
                    //������ ����������
                    gui.getScreen().getApplication().trajectories.remove(gui.getScreen().getApplication().trajectories.get(object));
                    gui.setSelectedObject(null);                    
                    gui.addSimState(object.getName() + " deleted");
                    gui.update();
                }
            });
            add(btnDelete);
            
        }
        
        @Override
        public void update() {
            txtName.setText(object.getName());
            Vector p = object.getPos();
            Vector v = object.getSpeed();
            txtX.setText(String.valueOf(p.x));
            txtY.setText(String.valueOf(p.y));
            txtVX.setText(String.valueOf(v.x));
            txtVY.setText(String.valueOf(v.y));
            txtM.setText(String.valueOf(object.getM()));
            txtR.setText(String.valueOf(object.getR()));
        }
    }
